<?php

defined('IN_IA') or exit('Access Denied');
require("../addons/ewei_shopv2/defines.php");
require("../addons/ewei_shopv2/core/inc/functions.php");

class Ewei_shopv2ModuleWxapp extends WeModuleWxapp
{
    /**
     * 获取openid
     */
    public function doPageOpenid()
    {
        global $_GPC;
        $code = $_GPC['code'];
        $system = pdo_fetch("select * from " . tablename('red_system'));
        if ($system) {
            $appid = $system['appid'];
            $secret = $system['appsecret'];
            $url = "https://api.weixin.qq.com/sns/jscode2session?appid=" . $appid . "&secret=" . $secret . "&js_code=" . $code . "&grant_type=authorization_code";
            $res = m('util')->httpRequest($url);
            if ($res) {
                $data['code'] = 1;
                $data['message'] = '获取成功';
                $data['result'] = $res;
            } else {
                $data['code'] = 0;
                $data['message'] = '获取失败';
            }
            echo json_encode($data);
        }

    }

    /**
     * 用户授权
     */
    public function doPageAuthorize()
    {
        global $_GPC, $_W;
        $openid = $_GPC['openid'];
        $info['avatar'] = $_GPC['avatar'];
        $info['nickname'] = $_GPC['nick_name'];
        $res = pdo_fetch("select id,openid,nickname,avatar,realname,mobile,weixin,account_id,account_price,`type`,is_check from " . tablename('ewei_shop_member') . " where openid = '" . $openid . "' and uniacid = " . $_W['uniacid']);
        if ($res) {
            $id = $res['id'];
            $res = pdo_update('ewei_shop_member', $info, array('id' => $res['id']));
        } else {
            $info['openid'] = $openid;
            $info['uniacid'] = $_W['uniacid'];
            $info['createtime'] = time();
            $res = pdo_insert('ewei_shop_member', $info);
            $id = pdo_insertid();
        }
        $res = pdo_fetch("select id,openid,nickname,avatar,realname,mobile,weixin,account_id,account_price,`type`,is_check from " . tablename('ewei_shop_member') . " where openid = '" . $openid . "' and uniacid = " . $_W['uniacid']);
        if ($res) {
            $data['code'] = 1;
            $data['message'] = '操作成功';
            $data['result'] = $id;
        } else {
            $data['code'] = 0;
            $data['message'] = '操作失败';
        }
        echo json_encode($data);
    }

    /**
     * 用户详情信息
     */
    public function doPageUserInfo()
    {
        global $_GPC;
        $user_id = $_GPC['user_id'];
        $res = pdo_fetch("select m.id,m.openid,m.nickname,m.avatar,m.realname,m.mobile,m.weixin,m.account_id,m.account_price,m.`type`,m.remark,m.is_check,a.sign_name from " . tablename('ewei_shop_member') . " as m left join " . tablename('red_account') . " as a on m.account_id = a.account_id where m.id = " . $user_id);
        if ($res) {
            $data['code'] = 1;
            $data['message'] = '获取成功';
            $data['result'] = $res;
        } else {
            $data['code'] = 1;
            $data['message'] = '获取失败';
            $data['result'] = array();
        }
        echo json_encode($data);
    }

    /**
     * 编辑用户信息
     */
    public function doPageEditUser()
    {
        global $_GPC;
        $user_id = $_GPC['user_id'];
        $info['realname'] = $realname = $_GPC['realname'];
        $info['mobile'] = $mobile = $_GPC['mobile'];
        $info['weixin'] = $weixin = $_GPC['weixin'];
        $info['account_id'] = $account_id = $_GPC['account_id'];
        $info['remark'] = $remark = $_GPC['remark'];


        if (empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        if (empty($realname)) {
            $data['code'] = 0;
            $data['message'] = '姓名不能为空';
            echo json_encode($data);
            exit;
        }
        if (empty($mobile)) {
            $data['code'] = 0;
            $data['message'] = '电话不能为空';
            echo json_encode($data);
            exit;
        }
        if (empty($weixin)) {
            $data['code'] = 0;
            $data['message'] = '微信不能为空';
            echo json_encode($data);
            exit;
        }
        if (empty($account_id)) {
            $data['code'] = 0;
            $data['message'] = '单位不能为空';
            echo json_encode($data);
            exit;
        }
        $userInfo = pdo_fetch("select * from " . tablename('ewei_shop_member') . " where id = " . $user_id);
        if ($userInfo) {
            if ($userInfo['account_id'] != $account_id) {
                $info['is_check'] = $is_check = 0;
            }
            $res = pdo_update('ewei_shop_member', $info, array('id' => $user_id));
            if ($res) {
                $data['code'] = 1;
                $data['message'] = '编辑成功';
            } else {
                $data['code'] = 0;
                $data['message'] = '编辑失败';
            }
        } else {
            $data['code'] = 0;
            $data['message'] = '编辑失败';
        }

        echo json_encode($data);

    }

    /**
     * 获取所有单位
     */
    public function doPageAccount()
    {
        global $_GPC;
        $keyword = trim($_GPC['keyword']);
        if ($keyword) {
            $where = " and sign_name like '%" . $keyword . "%'";
        }
        $res = pdo_fetchall("select * from " . tablename('red_account') . " where parent_id = 0 and is_stop = 0" . $where);
        if ($res) {
            $data['code'] = 1;
            $data['message'] = '获取成功';
            $data['result'] = $res;
        } else {
            $data['code'] = 1;
            $data['message'] = '获取失败';
            $data['result'] = array();
        }
        echo json_encode($data);
    }




    /**
     * 提现
     * $user_id
     * $money
     * $type==alipay||wechat
     */
    public function doPageTrash()
    {
        global $_GPC, $_W;
        $sysset = m('common')->getSysset();
        //是否需要提现审核
        $withdraw_review = $sysset['trade']['withdraw_review'];
        //提现手续费
        $withdraw_charge = $sysset['trade']['withdrawcharge'];
        $user_id = $_GPC['user_id'];
        $money = $_GPC['money'];
        $type = $_GPC['type'];
        $username = $_GPC['username'];
        $realname = $_GPC['realname'];
        if (empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        if (empty($type)) {
            $data['code'] = 0;
            $data['message'] = '请选择提现方式';
            echo json_encode($data);
            exit;
        }
        if ($type == 'alipay') {
            if (empty($username)) {
                $data['code'] = 0;
                $data['message'] = '请填写支付宝账户';
                echo json_encode($data);
                exit;
            }
            if (empty($realname)) {
                $data['code'] = 0;
                $data['message'] = '请填写支付宝真实姓名';
                echo json_encode($data);
                exit;
            }
        }
        if (empty($money)) {
            $data['code'] = 0;
            $data['message'] = '请输入金额';
            echo json_encode($data);
            exit;
        }
        $user_info = pdo_fetch("select m.id,m.openid,m.account_price from " . tablename('ewei_shop_member') . " as m where m.id = " . $user_id);
        if ($user_info) {
            if ($user_info['account_price'] >= $money) {
                $openid = $user_info['openid'];
                if ($withdraw_review == 1) {
                    //提现需要审核
                    $this->widthdrawReview($user_id, $money, $type, $username, $realname);
                } else {
                    //提现不需要审核
                    //如果设置手续费扣除手续费
                    if ($withdraw_charge > 0) {
                        $realmoney = $money - $money * $withdraw_charge / 100;
                    } else {
                        $realmoney = $money;
                    }
                    $realmoney = sprintf("%.2f", $realmoney);
                    if ($type == 'wechat') {
                        $res = m('finance')->wechatTransfers($openid, $realmoney * 100);
                        if ($res) {
                            if ($res['code'] == '200') {
                                $objectxml = (array)simplexml_load_string($res['content'], 'SimpleXMLElement', LIBXML_NOCDATA);
                                if ($objectxml) {
                                    $result_code = strtolower($objectxml['result_code']);
                                    if ($result_code == 'success') {
                                        $price = $user_info['account_price'] - $money;
                                        m('member')->setCredit2($openid, 'account_price', -$money, '微信提现无需审核');
                                        pdo_insert('red_finance_log', array('user_id' => $user_id, 'add_time' => time(), 'money' => $money, 'account_money' => $price, 'type' => 1));
                                        $log = array(
                                            'uniacid' => $_W['uniacid'],
                                            'openid' => $user_info['openid'],
                                            'type' => 1,
                                            'logno' => m("common")->createNO("member_log", "logno", "RW"),
                                            'title' => '余额提现不需审核',
                                            'createtime' => time(),
                                            'status' => 1,
                                            'money' => $money,
                                            'realmoney' => $realmoney,
                                            'charge' => $withdraw_charge ? $withdraw_charge : 0,
                                            'deductionmoney' => $money - $realmoney,
                                            'applytype' => 0
                                        );
                                        pdo_insert('ewei_shop_member_log', $log);
                                        $data['code'] = 1;
                                        $data['message'] = '提现成功';
                                        echo json_encode($data);
                                        exit;

                                    } else {
                                        $data['code'] = 0;
                                        $data['message'] = $objectxml['err_code_des'];
                                        echo json_encode($data);
                                        exit;
                                    }
                                } else {
                                    $data['code'] = 0;
                                    $data['message'] = '提现失败';
                                    echo json_encode($data);
                                    exit;
                                }
                            } else {
                                $data['code'] = 0;
                                $data['message'] = '提现失败';
                                echo json_encode($data);
                                exit;
                            }
                        }
                    }
                    //支付宝打款
                    if ($type == 'alipay') {
                        $sec = m('common')->getSec();
                        $sec = iunserializer($sec['sec']);
                        if (empty($sec['alipay_pay']['open'])) {
                            $data['code'] = 0;
                            $data['message'] = '处理失败';
                            echo json_encode($data);
                            exit;
                        }
                        $batch_no_money = $realmoney * 100;
                        $batch_no = 'D' . date('Ymd') . 'RW' . rand(1, 9999) . 'MONEY' . $batch_no_money . rand(1, 100);
                        $res = m('finance')->singleTransfer($batch_no, $username, $realname, $realmoney, '余额提现');
                        if ($res->code == 10000) {
                            $price = $user_info['account_price'] - $money;
                            m('member')->setCredit2($openid, 'account_price', -$money, '支付宝提现无需审核');
                            pdo_insert('red_finance_log', array('user_id' => $user_id, 'add_time' => time(), 'money' => $money, 'account_money' => $price, 'type' => 1));
                            $log = array(
                                'uniacid' => $_W['uniacid'],
                                'openid' => $user_info['openid'],
                                'type' => 1,
                                'logno' => m("common")->createNO("member_log", "logno", "RW"),
                                'title' => '余额提现不需审核',
                                'createtime' => time(),
                                'status' => 1,
                                'money' => $money,
                                'realmoney' => $realmoney,
                                'charge' => $withdraw_charge ? $withdraw_charge : 0,
                                'deductionmoney' => $money - $realmoney,
                                'applytype' => 2,
                                'alipay' => $username,
                                'realname' => $realname,
                            );
                            pdo_insert('ewei_shop_member_log', $log);

                            $data['code'] = 1;
                            $data['message'] = '提现成功';
                            echo json_encode($data);
                            exit;
                        } else {
                            $data['code'] = 0;
                            $data['message'] = $res->sub_msg;
                            echo json_encode($data);
                            exit;
                        }
                    }
                }
            } else {
                $data['code'] = 0;
                $data['message'] = '您输入的金额错误';
                echo json_encode($data);
                exit;
            }

        } else {
            $data['code'] = 0;
            $data['message'] = '处理失败';
            echo json_encode($data);
            exit;
        }
    }

    /**
     * 提现需要审核
     */
    protected function widthdrawReview($user_id = '', $money = '', $type = '', $username = '', $realname = '')
    {
        global $_GPC, $_W;
        $sysset = m('common')->getSysset();
        //提现手续费
        $withdraw_charge = $sysset['trade']['withdrawcharge'];
        if (empty($user_id) || empty($money) || empty($type)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        if ($type == 'alipay') {
            if (empty($username)) {
                $data['code'] = 0;
                $data['message'] = '请填写支付宝账户';
                echo json_encode($data);
                exit;
            }
            if (empty($realname)) {
                $data['code'] = 0;
                $data['message'] = '请填写支付宝真实姓名';
                echo json_encode($data);
                exit;
            }
        }
        $user_info = pdo_fetch("select m.id,m.openid,m.account_price from " . tablename('ewei_shop_member') . " as m where m.id = " . $user_id);
        if ($user_info) {
            if ($user_info['account_price'] >= $money) {
                if ($withdraw_charge > 0) {
                    $realmoney = $money - $money * $withdraw_charge / 100;
                } else {
                    $realmoney = $money;
                }
                $realmoney = sprintf("%.2f", $realmoney);
                if ($type == 'wechat') {
                    $price = $user_info['account_price'] - $money;
                    m('member')->setCredit2($user_info['openid'], 'account_price', -$money, '微信提现需审核');
                    pdo_insert('red_finance_log', array('user_id' => $user_id, 'add_time' => time(), 'money' => $money, 'account_money' => $price, 'type' => 1));
                    $log = array(
                        'uniacid' => $_W['uniacid'],
                        'openid' => $user_info['openid'],
                        'type' => 1,
                        'logno' => m("common")->createNO("member_log", "logno", "RW"),
                        'title' => '余额提现需审核',
                        'createtime' => time(),
                        'status' => 0,
                        'money' => $money,
                        'realmoney' => $realmoney,
                        'charge' => $withdraw_charge ? $withdraw_charge : 0,
                        'deductionmoney' => $money - $realmoney,
                        'applytype' => 0
                    );
                    pdo_insert('ewei_shop_member_log', $log);
                    $data['code'] = 1;
                    $data['message'] = '提现申请成功请等待审核';
                    echo json_encode($data);
                    exit;
                }
                if ($type == 'alipay') {
                    $sec = m('common')->getSec();
                    $sec = iunserializer($sec['sec']);
                    if (empty($sec['alipay_pay']['open'])) {
                        $data['code'] = 0;
                        $data['message'] = '处理失败';
                        echo json_encode($data);
                        exit;
                    }
                    $price = $user_info['account_price'] - $money;
                    m('member')->setCredit2($user_info['openid'], 'account_price', -$money, '支付宝提现需审核');
                    pdo_insert('red_finance_log', array('user_id' => $user_id, 'add_time' => time(), 'money' => $money, 'account_money' => $price, 'type' => 1));
                    $log = array(
                        'uniacid' => $_W['uniacid'],
                        'openid' => $user_info['openid'],
                        'type' => 1,
                        'logno' => m("common")->createNO("member_log", "logno", "RW"),
                        'title' => '余额提现需审核',
                        'createtime' => time(),
                        'status' => 0,
                        'money' => $money,
                        'realmoney' => $realmoney,
                        'charge' => $withdraw_charge ? $withdraw_charge : 0,
                        'deductionmoney' => $money - $realmoney,
                        'applytype' => 2,
                        'alipay' => $username,
                        'realname' => $realname,
                    );
                    pdo_insert('ewei_shop_member_log', $log);
                    $data['code'] = 1;
                    $data['message'] = '提现申请成功请等待审核';
                    echo json_encode($data);
                    exit;
                }
            } else {
                $data['code'] = 0;
                $data['message'] = '您输入的金额错误';
                echo json_encode($data);
                exit;
            }

        } else {
            $data['code'] = 0;
            $data['message'] = '处理失败';
            echo json_encode($data);
            exit;
        }

    }

    /**
     * 活动列表
     */
    public
    function doPageActivityList()
    {
        global $_GPC;
        $user_id = intval($_GPC['user_id']);
        $type = intval($_GPC['type']) ? intval($_GPC['type']) : 1;
        if (empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        $today = strtotime(date('Y-m-d', time()));
        $where = ' where a.is_delete = 0';
        if ($type == 1) {
            //进行中
            $where .= " and a.start_time<=" . $today . " and a.end_time>=" . $today;
        } else {
            $where .= "  and a.end_time < " . $today;
        }

        //获取该会员所属门店
        $userInfo = pdo_fetch("select account_id from " . tablename('ewei_shop_member') . " where id = " . $user_id);
        if ($userInfo) {
            $account_id = $userInfo['account_id'];
            $where .= "  and c.account_id = " . $account_id;
            $res = pdo_fetchall("select * from " . tablename('red_activity') . ' as a left join ' . tablename('red_active_account') . 'as c on a.activity_id =c.activity_id' . $where);
            foreach ($res as $key => $value) {
                $res[$key]['thumb'] = tomedia($value['thumb']);
                $goods = pdo_fetchall("select * from " . tablename('red_active_goods') . " where activity_id = " . $value['activity_id']);
                $res[$key]['count'] = count($goods);
            }

            if ($res) {
                $data['code'] = 1;
                $data['message'] = '获取成功';
                $data['result'] = $res;
            } else {
                $data['code'] = 1;
                $data['message'] = '获取失败';
                $data['result'] = array();
            }
        } else {
            $data['code'] = 0;
            $data['message'] = '未绑定门店';
        }
        echo json_encode($data);
        exit;
    }

    /**
     * 活动详情
     */
    public
    function doPageActivityDetail()
    {
        global $_GPC;
        $user_id = intval($_GPC['user_id']);
        $activity_id = intval($_GPC['activity_id']);
        if (empty($user_id) || empty($activity_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        //获取该会员所属门店
        $userInfo = pdo_fetch("select account_id from " . tablename('ewei_shop_member') . " where id = " . $user_id);
        if ($userInfo) {
            $account_id = $userInfo['account_id'];
            $res = pdo_fetchall("select a.* from " . tablename('red_activity') . ' as a left join ' . tablename('red_active_account') . 'as c on a.activity_id =c.activity_id where c.account_id = ' . $account_id);
            $activitys = array();
            foreach ($res as $value) {
                $activitys[] = $value['activity_id'];
            }
            $is_in = in_array($activity_id, $activitys);
            if (!$is_in) {
                $data['code'] = 0;
                $data['message'] = '您没有权限查看此活动';
                echo json_encode($data);
                exit;
            }
            $result = pdo_fetch("select a.*,c.account_id from " . tablename('red_activity') . " as a 
                           left join " . tablename('red_active_account') . " as c on a.activity_id = c.activity_id
                           where c.account_id = " . $account_id . " and a.activity_id = " . $activity_id);

            $goods = pdo_fetchall("select o.*,o.price as goods_price, g.* from " . tablename('red_active_goods') . " as g left join " . tablename('red_goods') . " as o on g.goods_id = o.goods_id where g.activity_id = " . $activity_id);
            foreach ($goods as $key => $value) {
                $amountInfo = pdo_fetch("select * from " . tablename('red_detail') . " where goods_id = " . $value['goods_id'] . " and account_id = " . $account_id);
                $goods[$key]['amount'] = 0;
                if ($amountInfo) {
                    $goods[$key]['amount'] = $amountInfo['amount'];
                }
                $goods[$key]['thumb'] = tomedia($value['thumb']);

            }

            $result['goods'] = $goods;

            if ($res) {
                $data['code'] = 1;
                $data['message'] = '获取成功';
                $data['result'] = $result;
            } else {
                $data['code'] = 1;
                $data['message'] = '获取失败';
                $data['result'] = array();
            }
        } else {
            $data['code'] = 0;
            $data['message'] = '数据获取失败';
        }
        echo json_encode($data);
        exit;
    }

    /**
     * 判断该商品是否有库存
     */
    public
    function doPageHasAmount()
    {
        global $_GPC;
        $bar_code = trim($_GPC['bar_code']);
        $user_id = intval($_GPC['user_id']);

        if (empty($bar_code) || empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        //获取该会员所属门店
        $userInfo = pdo_fetch("select account_id from " . tablename('ewei_shop_member') . " where id = " . $user_id);
        if ($userInfo) {
            $account_id = $userInfo['account_id'];
            $goodsInfo = pdo_fetch("select * from " . tablename('red_goods') . "  where is_delete = 0  and bar_code = '" . $bar_code . "'");
            if ($goodsInfo) {
                $goods_id = $goodsInfo['goods_id'];
                $res = pdo_fetch("select * from " . tablename('red_detail') . " where goods_id = " . $goods_id . " and account_id = " . $account_id);
                if ($res) {
                    $data['code'] = 1;
                    $data['message'] = '获取成功';
                    $data['result'] = $res;
                } else {
                    $data['code'] = 0;
                    $data['message'] = '获取失败';
                }
            } else {
                $data['code'] = 0;
                $data['message'] = '系统中不存在该商品';
            }
        } else {
            $data['code'] = 0;
            $data['message'] = '获取失败';
        }
        echo json_encode($data);
        exit;
    }

    /**
     *  某用户可选的所有商品
     */
    public function doPageAllGoods(){
        global $_GPC;
        $user_id = intval($_GPC['user_id']);
        $sale_time = strtotime($_GPC['sale_time']);
        $keyword = trim($_GPC['keyword']);

        if(empty($user_id)){
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        if(empty($sale_time)){
            $data['code'] = 0;
            $data['message'] = '必须选择销售时间';
            echo json_encode($data);
            exit;
        }
        if($keyword){
            $where1 = " and goods_name like '%".$keyword."%'";
        }
        $goods_arr = array();
        //获取该会员所属门店
        $userInfo = pdo_fetch("select account_id from ".tablename('ewei_shop_member')." where id = ".$user_id);

        if($userInfo){
            $account_id = $userInfo['account_id'];
            //获取销售时间（这一天）该单位参加的所有活动的商品
            $where = " a.start_time <= ".$sale_time." and a.end_time >= ".$sale_time;
            $goods = pdo_fetchall("select a.sort,g.* from ".tablename('red_activity')." as a left join ".tablename('red_active_account')." as c on a.activity_id = c.activity_id left join ".tablename('red_active_goods')." as g on a.activity_id = g.activity_id where c.account_id = ".$account_id." and ".$where." and is_delete = 0 group by g.goods_id");


            foreach ($goods as $key=>$value){

                //获取该商品该时间段的活动排序最高的
                $active_goods =  pdo_fetch("select a.* from ".tablename('red_activity')." as a left join ".tablename('red_active_goods')." g on a.activity_id = g.activity_id left join ".tablename('red_active_account')." c on c.activity_id = a.activity_id where c.account_id = ".$account_id." and  a.is_delete = 0 and  g.goods_id = ".$value['goods_id']." and ".$where ." order by a.sort desc");

                //获取排序最高的该时间段该商品的所有活动且佣金最高的活动
                $price =0;
                $activity_id = 0;
                if($active_goods){
                    $sort = $active_goods['sort'];
                    $highest_active = pdo_fetch("select g.* from ".tablename('red_activity')." as a left join ".tablename('red_active_goods')." as g on a.activity_id = g.activity_id where a.is_delete = 0 and a.sort = ".$sort." and g.goods_id = ".$value['goods_id']." and ".$where." and a.activity_id = ".$active_goods['activity_id']." order by g.price desc");

                    $price = $highest_active['price'];
                    $activity_id = $highest_active['activity_id'];
                }
                //处理商品数据
                $info = pdo_fetch("select *,price as goods_price from ".tablename('red_goods')." where goods_id = ".$value['goods_id'].$where1);

                $detail = pdo_fetch("select * from ".tablename('red_detail')." where goods_id = ".$value['goods_id']." and account_id = ".$account_id);
                if($detail&&$info){
                    $info['thumb'] = tomedia($info['thumb']);
                    $info['amount'] = $detail['amount'];
                    $info['price'] = $price;
                    $info['activity_id'] = $activity_id;
                    $goods_arr[] = $info;
                }

            }
            $data['code'] = 1;
            $data['message'] = '获取成功';
            $data['result'] = $goods_arr;
        }else{
            $data['code'] = 0;
            $data['message'] = '获取失败';
        }
        echo json_encode($data);
        exit;
    }

    /**
     *  扫码获取商品
     */
    public
    function doPageScanGoods()
    {
        global $_GPC;
        $user_id = intval($_GPC['user_id']);
        $sale_time = strtotime($_GPC['sale_time']);
        $keyword = trim($_GPC['bar_code']);

        if (empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        if (empty($sale_time)) {
            $data['code'] = 0;
            $data['message'] = '必须选择销售时间';
            echo json_encode($data);
            exit;
        }
        if (empty($keyword)) {
            $data['code'] = 0;
            $data['message'] = '条码不能为空';
            echo json_encode($data);
            exit;
        }
        $where1 = '';
        if ($keyword) {
            $where1 = " and s.bar_code like '%" . $keyword . "%'";
        }
        //获取该会员所属门店
        $userInfo = pdo_fetch("select account_id from " . tablename('ewei_shop_member') . " where id = " . $user_id);
        if ($userInfo) {
            $account_id = $userInfo['account_id'];
            //获取销售时间（这一天）该单位所有参加活动的相似条码的商品
            $where = " a.start_time <= " . $sale_time . " and a.end_time >= " . $sale_time;
            $goods = pdo_fetchall("select a.sort,g.*,s.goods_name,s.bar_code from " . tablename('red_activity') . " as a left join " . tablename('red_active_account') . " as c on a.activity_id = c.activity_id left join " . tablename('red_active_goods') . " as g on a.activity_id = g.activity_id left join " . tablename('red_goods') . " as s on g.goods_id = s.goods_id where c.account_id = " . $account_id . " and " . $where . $where1 . " and a.is_delete = 0 group by g.goods_id");
            $goods_arr = array();
            foreach ($goods as $g) {
                $info = pdo_fetch("select a.sort,g.*,s.goods_name,s.bar_code from " . tablename('red_activity') . " as a  left join " . tablename('red_active_goods') . " as g on a.activity_id = g.activity_id left join " . tablename('red_goods') . " as s on g.goods_id = s.goods_id left join " . tablename('red_detail') . " as d on s.goods_id = d.goods_id  where d.account_id = " . $account_id . " and " . $where . " and s.bar_code = '" . $g['bar_code'] . "' and a.is_delete = 0 and d.detail_id >0");
                if ($info) {
                    $goods_arr[] = $info;
                }
            }
            $data['code'] = 1;
            $data['message'] = '获取成功';
            $data['result'] = $goods_arr;
        } else {
            $data['code'] = 0;
            $data['message'] = '获取失败';
        }
        echo json_encode($data);
        exit;
    }

    /**
     * 提单
     */
    public
    function doPageCommitOrder()
    {
        global $_GPC;
        $info['user_id'] = $user_id = intval($_GPC['user_id']);

        if (empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        $info['sale_time'] = $sale_time = strtotime($_GPC['sale_time']);
        if (empty($sale_time)) {
            $data['code'] = 0;
            $data['message'] = '销售时间不能为空';
            echo json_encode($data);
            exit;
        }
        $goods_ids = $_GPC['goods_ids'];
        $amounts = $_GPC['amounts'];
        $prices = $_GPC['prices'];
        $info['all_money'] = $all_price = $_GPC['all_money'];
        $info['pic'] = $pic = $_GPC['pic'];

        if (empty($goods_ids)) {
            $data['code'] = 0;
            $data['message'] = '产品不能为空';
            echo json_encode($data);
            exit;
        }
        if (empty($amounts)) {
            $data['code'] = 0;
            $data['message'] = '产品数量不能为空';
            echo json_encode($data);
            exit;
        }
        if (empty($prices)) {
            $data['code'] = 0;
            $data['message'] = '佣金价格不能为空';
            echo json_encode($data);
            exit;
        }
        $goods_id_arr = array_filter(explode(';', $goods_ids));
        $amount_arr = array_filter(explode(';', $amounts));
        $price_arr = array_filter(explode(';', $prices));

        //获取该会员所属门店
        $userInfo = pdo_fetch("select account_id from " . tablename('ewei_shop_member') . " where id = " . $user_id);
        if ($userInfo) {
            $account_id = $userInfo['account_id'];
            $price = 0;
            foreach ($goods_id_arr as $key => $goods_id) {
                //获取该商品参与的优先级最高的活动佣金
                $active = pdo_fetch("select * from " . tablename('red_activity') . " as a left join " . tablename('red_active_goods') . " as g on a.activity_id = g.activity_id left join " . tablename('red_active_account') . " as c on a.activity_id = c.activity_id where a.is_delete = 0 and g.goods_id = " . $goods_id . " and c.account_id = " . $account_id . " order by a.sort desc");
                if ($active) {
                    $price += $active['price'] * $amount_arr[$key];
                }
            }

            if ($price != $all_price) {
                $data['code'] = 0;
                $data['message'] = '佣金价格有误';
                echo json_encode($data);
                exit;
            }
            $account_name = pdo_fetchcolumn('select sign_name from '.tablename('red_account').' where account_id = '.$userInfo['account_id']);
            $info['add_time'] = time();
            $info['order_num'] = $this->get_record_sn();
            $info['account_id'] = $userInfo['account_id'];
            $info['sign_name'] = $account_name;

            pdo_insert('red_order', $info);
            $order_id = pdo_insertid();
            if ($order_id) {
                foreach ($goods_id_arr as $key => $goods_id) {
                    $order_detail = array();
                    $order_detail['goods_id'] = $goods_id;
                    $order_detail['num_id'] = $key + 1;
                    $order_detail['amount'] = $amount_arr[$key];
                    $order_detail['price'] = $price_arr[$key];
                    $order_detail['order_id'] = $order_id;
                    $order_detail['single_price'] = $price_arr[$key] / $amount_arr[$key];
                    pdo_query('update '.tablename('red_detail').' set amount = amount - '.$amount_arr[$key] .' where account_id = '.$account_id .' and goods_id= '. $goods_id);
                    pdo_insert('red_order_detail', $order_detail);
                }
                $data['code'] = 1;
                $data['message'] = '提单成功';
                echo json_encode($data);
                exit;
            } else {
                $data['code'] = 0;
                $data['message'] = '提单失败';
                echo json_encode($data);
                exit;
            }
        } else {
            $data['code'] = 0;
            $data['message'] = '提单失败';
            echo json_encode($data);
            exit;
        }
    }

    /**
     * 提单列表
     */
    public
    function doPageOrderList()
    {
        global $_GPC;
        $user_id = intval($_GPC['user_id']);
        $status = intval($_GPC['status']) ? intval($_GPC['status']) : 0;
        if (empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        $where = " and d.is_delete = 0  and d.status = " . $status;
        $order_list = pdo_fetchall("select d.*,o.order_num,o.add_time from " . tablename('red_order_detail') . " as d left join " . tablename('red_order') . " as o on d.order_id = o.order_id where o.user_id = " . $user_id . $where);
        foreach ($order_list as $key => $order) {
            $order_list[$key]['order_num'] = $order['order_num'] . '-' . $order['num_id'];
        }
        if ($order_list) {
            $data['code'] = 1;
            $data['message'] = '获取成功';
            $data['result'] = $order_list;
        } else {
            $data['code'] = 1;
            $data['message'] = '获取失败';
            $data['result'] = array();

        }
        echo json_encode($data);
    }

    /**
     * 提单详情
     */
    public
    function doPageOrderDetail()
    {
        global $_GPC;
        $id = $_GPC['id'];
        if (empty($id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        $order_detail = pdo_fetch("select d.*,o.order_num,o.add_time,g.price as goods_price,g.goods_name,g.thumb from " . tablename('red_order_detail') . " as d left join " . tablename('red_order') . " as o on d.order_id = o.order_id left join " . tablename('red_goods') . " as g  on d.goods_id = g.goods_id  where d.id = " . $id);
        if ($order_detail) {
            $order_detail['order_num'] = $order_detail['order_num'] . '-' . $order_detail['num_id'];
            $order_detail['thumb'] = tomedia($order_detail['thumb']);
            $data['code'] = 1;
            $data['message'] = '获取成功';
            $data['result'] = $order_detail;
        } else {
            $data['code'] = 0;
            $data['message'] = '获取失败';
        }
        echo json_encode($data);
    }

    /**
     * 某商品详情
     */
    public
    function doPageGoodsDetail()
    {
        global $_GPC;
        $goods_id = intval($_GPC['goods_id']);
        $user_id = intval($_GPC['user_id']);
        $sale_time = strtotime($_GPC['sale_time']);

        if (empty($goods_id) || empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        if (empty($sale_time)) {
            $data['code'] = 0;
            $data['message'] = '必须选择销售时间';
            echo json_encode($data);
            exit;
        }
        //获取该会员所属门店
        $userInfo = pdo_fetch("select account_id from " . tablename('ewei_shop_member') . " where id = " . $user_id);
        if ($userInfo) {
            $goods = pdo_fetch("select *,price as goods_price from " . tablename('red_goods') . " where goods_id = " . $goods_id);
            if (!$goods) {
                $data['code'] = 0;
                $data['message'] = '没有该商品，请联系客服';
                echo json_encode($data);
                exit;
            }
            $account_id = $userInfo['account_id'];
            //获取销售时间（这一天）该单位该商品参加的所有活动优先级最高的
            $where = " and a.start_time <= " . $sale_time . " and a.end_time >= " . $sale_time;
            $active = pdo_fetch("select * from " . tablename('red_activity') . " as a left join " . tablename('red_active_goods') . " as g on a.activity_id = g.activity_id left join " . tablename('red_active_account') . " as c on a.activity_id = c.activity_id where a.is_delete =0 and g.goods_id = " . $goods_id . " and c.account_id = " . $account_id . $where . " order by a.sort desc");
            if ($active) {
                //获取排序最高的该时间段该商品的所有活动且佣金最高的活动
                $sort = $active['sort'];
                $highest_active = pdo_fetch("select g.* from " . tablename('red_activity') . " as a left join " . tablename('red_active_goods') . " as g on a.activity_id = g.activity_id left join " . tablename('red_active_account') . " as c on a.activity_id = c.activity_id where a.is_delete = 0 and a.sort = " . $sort . " and g.goods_id = " . $goods_id . " and c.account_id = " . $account_id . $where . " order by g.price desc");
                $price = $highest_active['price'];

                $detail = pdo_fetch("select * from " . tablename('red_detail') . " where account_id = " . $account_id . " and goods_id = " . $goods_id);
                if (!$detail) {
                    $data['code'] = 0;
                    $data['message'] = '该商品没有库存';
                    echo json_encode($data);
                    exit;
                }
                $goods['amount'] = $detail['amount'];
                $goods['price'] = $price;
                $data['code'] = 1;
                $data['message'] = '获取成功';
                $data['result'] = $goods;
            } else {
                $data['code'] = 0;
                $data['message'] = '该商品没有参与活动';
            }
        } else {
            $data['code'] = 0;
            $data['message'] = '获取失败';
        }
        echo json_encode($data);
        exit;

    }

    /**
     * 删除提单
     */
    public
    function doPageOrderDel()
    {
        global $_GPC;
        $id = $_GPC['id'];
        $user_id = $_GPC['user_id'];
        if (empty($id) || empty($user_id)) {
            $data['code'] = 0;
            $data['message'] = '系统繁忙';
            echo json_encode($data);
            exit;
        }
        $detail = pdo_fetch("select d.status,d.goods_id,o.account_id,d.amount from " . tablename('red_order_detail') . " as d left join " . tablename('red_order') . " as o on d.order_id = o.order_id where d.is_delete = 0 and  o.user_id = " . $user_id . " and d.id = " . $id);
        if (empty($detail)) {
            $data['code'] = 0;
            $data['message'] = '单子出错';
            echo json_encode($data);
            exit;
        }
        if ($detail['status'] != 0||empty($detail['goods_id'])||empty($detail['account_id'])) {
            $data['code'] = 0;
            $data['message'] = '此状态不能删除单子';
            echo json_encode($data);
            exit;
        }
        $res = pdo_update('red_order_detail', array('is_delete' => 1), array('id' => $id));
        pdo_query('update '.tablename('red_detail').' set amount = amount +'.$detail['amount'].' where goods_id = :goods_id and account_id = :account_id',array(':goods_id'=>$detail['goods_id'],':account_id'=>$detail['account_id']));
        if ($res) {
            $data['code'] = 1;
            $data['message'] = '删除成功';
        } else {
            $data['code'] = 0;
            $data['message'] = '获取失败';
        }
        echo json_encode($data);
        exit;
    }

    /**
     * 上传图片
     */
    public
    function doPageUpload()
    {
        global $_W, $_GPC;
        $uptypes = array('image/jpg', 'image/jpeg', 'image/png', 'image/pjpeg', 'image/gif', 'image/bmp', 'image/x-png');
        $max_file_size = 2000000;     //上传文件大小限制, 单位BYTE
        $destination_folder = "../attachment/upload/"; //上传文件路径
        if (!is_uploaded_file($_FILES["upfile"]['tmp_name'])) {
            $data['path'] = '';
            $data['code'] = 0;
            $data['message'] = '图片不存在！';
            echo json_encode($data);
            exit;
        }
        $file = $_FILES["upfile"];

        if ($max_file_size < $file["size"]) {
            $data['path'] = '';
            $data['code'] = 0;
            $data['message'] = '文件过大！';
            echo json_encode($data);
            exit;
        }
        //兼容处理部分安卓机是字节流类型
        if ($file['type'] == 'application/octet-stream') {
            $file['type'] = 'image/' . pathinfo($file['name'], PATHINFO_EXTENSION);
        }
        if (!in_array($file["type"], $uptypes)) {
            $data['path'] = '';
            $data['code'] = 0;
            $data['info'] = $file["type"];
            $data['message'] = '文件类型不符！';
            echo json_encode($data);
            exit;
        }
        $filename = $file["tmp_name"];
        $image_size = getimagesize($filename);
        $pinfo = pathinfo($file["name"]);
        $ftype = $pinfo['extension'];
        $destination = $destination_folder . str_shuffle(time() . rand(111111, 999999)) . "." . $ftype;
        if (file_exists($destination) && $overwrite != true) {
            $data['path'] = '';
            $data['code'] = 0;
            $data['message'] = '已存在同名文件！';
            echo json_encode($data);
            exit;
        }
        if (!move_uploaded_file($filename, $destination)) {
            $data['path'] = '';
            $data['code'] = 0;
            $data['message'] = '移动文件出错！';
            echo json_encode($data);
            exit;
        }
        $pinfo = pathinfo($destination);
        $fname = $pinfo['basename'];
        $data['origin_path'] = $destination;
        $data['path'] = 'https://' . $_SERVER['HTTP_HOST'] . substr($destination, 2);
        $data['code'] = 1;
        $data['message'] = '上传成功！';
        echo json_encode($data);
        @require_once(IA_ROOT . '/framework/function/file.func.php');
        @$filename = $fname;
        @file_remote_upload($filename);
    }

//生成单号
    function get_record_sn()
    {
        return date('YmdHis') . $this->create_random(5, '0123456789');
    }

//创建随机数
    function create_random($length, $char_str = 'abcdefghijklmnopqrstuvwxyz0123456789')
    {
        $hash = '';
        $chars = $char_str;
        $max = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $hash .= substr($chars, (rand(0, 1000) % $max), 1);
        }
        return $hash;
    }
}