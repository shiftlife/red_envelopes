<?php
if (!defined('IN_IA')) {
	exit('Access Denied');
}

class Index_EweiShopV2Page extends ComWebPage
{
	public function __construct($_com = 'verify')
	{
		parent::__construct($_com);
	}

	public function main()
	{
		global $_W;
		global $_GPC;
		$today = strtotime(date('Y-m-d',time()));
		$where = " and o.start_time <= ".$today." and o.end_time >= ".$today;
		$pindex = max(1, intval($_GPC['page']));
		$psize = 20;
		$keyword = $_GPC['keyword'];
		$condition = ' is_delete = 0 and parent_id = 0';
		$condition .= " and (sign_name like '%".$keyword."%' or sign_num like '%".$keyword."%' or contact_tel like '%".$keyword."%')";
		$sql = 'SELECT * FROM ' . tablename('red_account') . (' WHERE ' . $condition . ' ORDER BY account_id desc');
        $sql .= ' LIMIT ' . ($pindex - 1) * $psize . ',' . $psize;
		$sql_count = 'SELECT count(1) FROM ' . tablename('red_account') . (' WHERE ' . $condition);
		$total = pdo_fetchcolumn($sql_count);
		$pager = pagination2($total, $pindex, $psize);
		$list = pdo_fetchall($sql);
		foreach ($list as $key=>$value){
			if($value['is_bind'] == 1){
                $all = pdo_fetchall("select * from ".tablename('red_account')." where parent_id = ".$value['account_id']);
                $sign_nums = '';
                foreach ($all as $item){
                    $sign_nums .= $item['sign_num'].'</br>';
				}
                $list[$key]['sign_num'] =	$sign_nums;
			}else{
                $list[$key]['sign_num'] =  $value['sign_num'];
			}
			$detail_num = pdo_fetch("select COUNT(*) as c from ".tablename('red_detail')." where account_id = ".$value['account_id']);
			$list[$key]['account_num'] = $detail_num['c'];
			//店员数
			$member_num = pdo_fetch("select count(*) as c from ".tablename('ewei_shop_member').' where account_id = '.$value['account_id']);
            $list[$key]['member_num'] = $member_num['c'];
            //进行中活动
            $activity = pdo_fetchall("select * from ".tablename('red_active_account')." as a left join ".tablename('red_activity')." as o on a.activity_id = o.activity_id where account_id = ". $value['account_id'].$where);
            $list[$key]['active_num'] = count($activity);
        }
		include $this->template();
	}

    /**
     * 合并门店
     */
    public function combind(){
        global $_W;
        global $_GPC;
        if ($_W['ispost']) {
            if (empty($_GPC['sign_name'])) {
                show_json(0, '单位名称不能空');
            }
//            if (empty($_GPC['sign_num'])) {
//                show_json(0, '单位编码不能为空');
//            }
            $account_ids = $_GPC['account_id'];
            if (empty($account_ids)) {
                show_json(0, '合并门店不能为空');
            }
//            $data['sign_num'] = trim($_GPC['sign_num']);
            $data['sign_name'] = trim($_GPC['sign_name']);
            $data['contact_name'] = trim($_GPC['contact_name']);
            $data['contact_tel'] = trim($_GPC['contact_tel']);
            $data['mobile_tel'] = trim($_GPC['mobile_tel']);
            $data['address'] = trim($_GPC['address']);
            $data['limit_time'] = trim($_GPC['limit_time']);
            $data['area'] = trim($_GPC['area']);
            $data['remark'] = trim($_GPC['remark']);
            $data['is_stop'] = intval($_GPC['is_stop']);
            $data['is_bind'] = 1;
			pdo_insert('red_account', $data);
			$id = pdo_insertid();
			foreach ($account_ids as $account_id){
			     //账户关系修改
				 pdo_update("red_account",array('parent_id'=>$id),array('account_id'=>$account_id));
			    //门店商品关系修改
				$goods = pdo_fetchall("select * from ".tablename('red_detail')." where account_id = ".$account_id);
				foreach ($goods as $item){
					 $res = pdo_fetch("select * from ".tablename('red_detail')." where account_id = ".$id." and goods_id = ".$item['goods_id']);
					 if($res){
					     $amount = $item['amount'] + $res['amount'];
                         pdo_update('red_detail',array('account_id'=>$id,'amount'=>$amount),array('detail_id'=>$item['detail_id']));
                     }else{
                         //更新
                         pdo_update('red_detail',array('account_id'=>$id),array('detail_id'=>$item['detail_id']));
					 }
				}
				//店员关系修改
				$members = pdo_fetchall("select * from ".tablename('ewei_shop_member')." where account_id = ".$account_id);
				foreach ($members as $value){
                    pdo_update("ewei_shop_member",array('account_id'=>$id),array('id'=>$value['id']));
				}
			}
			//处理相同商品的数据
			$res = pdo_fetchall("select * from ".tablename('red_detail')." where account_id = ".$id." group by goods_id having count(goods_id)>1");
            foreach ($res as $value){
            	$details = pdo_fetchall("select * from ".tablename('red_detail')." where account_id = ".$value['account_id']." and goods_id = ".$value['goods_id']);
            	$amount = 0;
            	foreach ($details as $key=>$d){
            		$amount += $d['amount'];
            		if($key!=0){
            			pdo_delete('red_detail',array('detail_id'=>$d['detail_id']));
					}
				}
            	pdo_update('red_detail',array('amount'=>$amount),array('detail_id'=>$details[0]['detail_id']));
			}

            show_json(1, array('url' => webUrl('store')));
        }
        include $this->template();
    }
    public function store()
    {
        global $_W,$_GPC;
        if ($_W['ispost']) {
            $is_cover = intval($_GPC['is_cover']);
            if(empty($is_cover)){
                $this->message('是否覆盖不能为空');
            }
            $excel = m('excel')->import('excelfile');
            $time = time();
            $problem_data = m('store')->import_deal($excel,$is_cover,$time);
            $pro_count = count($problem_data['problem_data']);
            $suc_count = $problem_data['corret_count'];
            $all_count = $pro_count+$suc_count;
            if($pro_count>0){
                $arr = array('message'=>'导入数据共' . $all_count.'条，成功导入了'.$suc_count.'条，问题数据有'.$pro_count.'条','buttontext'=>'导出错误数据','type'=>1,'url'=>webUrl('tool/export',array('mark'=>$time)));
            }else{
                $arr = array('message'=>'导入数据共' . $all_count.'条，成功导入了'.$suc_count.'条，问题数据有'.$pro_count.'条');
            }
            $this->message($arr,'exit');
        }
        $res = pdo_fetchall("select * from ".tablename('red_account_log'));
        $pro_status = 0;
        if($res){
            $pro_status = 1;
        }
        include $this->template('tool/work_export');
    }

    /**
     * 商品列表
     */
    public function goodslist(){
        global $_GPC;
        $keyword = trim($_GPC['keyword']);
        if($keyword){
            $where = " and (goods_name like '%".$keyword."%' or bar_code like '%".$keyword."%')";
        }

        $pindex = max(1, intval($_GPC['page']));
        $psize = 20;
        $sql = "select * from ".tablename('red_goods')." where is_delete = 0".$where;
        $sql .= ' LIMIT ' . ($pindex - 1) * $psize . ',' . $psize;
        $sql_count = 'SELECT count(1) FROM ' . tablename('red_goods') . (' WHERE   is_delete = 0 ' . $where);
        $total = pdo_fetchcolumn($sql_count);
        $pager = pagination2($total, $pindex, $psize);
        $list = pdo_fetchall($sql);
        include $this->template('goods/goodslist');
    }


    /**
     * 商品导入
     */
    public function goods(){
        global $_W,$_GPC;
        if ($_W['ispost']) {
            $is_cover = intval($_GPC['is_cover']);
            if(empty($is_cover)){
                $this->message('是否覆盖不能为空');
            }
            $time = time();
            $excel = m('excel')->import('excelfile');
            $problem_data = m('store')->import_goods($excel,$is_cover,$time);
            $pro_count = count($problem_data['problem_data']);
            $suc_count = $problem_data['corret_count'];
            $all_count = $pro_count+$suc_count;
            if($pro_count>0){
                $arr = array('message'=>'导入数据共' . $all_count.'条，成功导入了'.$suc_count.'条，问题数据有'.$pro_count.'条','buttontext'=>'导出错误数据','type'=>1,'url'=>webUrl('tool/goods_export',array('mark'=>$time)));
            }else{
                $arr = array('message'=>'导入数据共' . $all_count.'条，成功导入了'.$suc_count.'条，问题数据有'.$pro_count.'条');
            }
            $this->message($arr,'exit');
        }
        include $this->template('tool/goods');
    }
    /**
     * 编辑商品
     */
    public function goodsedit(){
        global $_GPC,$_W;
        $goods_id = $_GPC['goods_id'];
        if($_W['ispost']){
            if($goods_id){
                $data = array('cat_id'=>$_GPC['cat_id'],'brand_id'=>$_GPC['brand_id'],'goods_name'=>$_GPC['goods_name'],'thumb'=>$_GPC['thumb'],'bar_code'=>$_GPC['bar_code'],'price'=>$_GPC['price'],'normal_size'=>$_GPC['normal_size'],'status'=>$_GPC['status']);
                $res =  pdo_update('red_goods',$data,array('goods_id'=>$goods_id));
            }else{
                $data = array('cat_id'=>$_GPC['cat_id'],'brand_id'=>$_GPC['brand_id'],'goods_name'=>$_GPC['goods_name'],'thumb'=>$_GPC['thumb'],'bar_code'=>$_GPC['bar_code'],'price'=>$_GPC['price'],'normal_size'=>$_GPC['normal_size'],'status'=>$_GPC['status']);
                $res = pdo_insert('red_goods',$data);
            }
            if($res){
                show_json(1,'操作成功');
            }else{
                show_json(0,'操作失败');
            }
        }
        $categreys = array();
        $categrey = pdo_fetchall("select * from ".tablename('red_category')." where parent_id = 0");
        foreach ($categrey as $cate){
        	$categreys[] = $cate;
        	$next = pdo_fetchall("select * from ".tablename('red_category')." where parent_id = ".$cate['cat_id']);
        	foreach ($next as $n){
        		$n['type_name'] = $cate['type_name'].'-'.$n['type_name'];
                $categreys[] = $n;
			}
		}
        $brands = pdo_fetchall("select * from ".tablename('red_brand'));

        $item = pdo_fetch("select * from ".tablename('red_goods')." where goods_id = ".$goods_id);
        include $this->template('goods/goodsedit');
    }
	public function add()
	{
		$this->post();
	}

	public function edit()
	{
		$this->post();
	}

	protected function post()
	{
		global $_W;
		global $_GPC;
		$account_id = intval($_GPC['ac_id']);
		if ($_W['ispost']) {
			$accountInfo = pdo_fetch("select * from ".tablename('red_account')." where account_id = ".$account_id);
			if($accountInfo){
				//编辑
				if($accountInfo['is_bind']==0){
                    if (empty($_GPC['sign_name'])) {
                        show_json(0, '单位名称不能空');
                    }
                    $data['sign_num'] = trim($_GPC['sign_num']);

                }
			}else{
				//添加
                if (empty($_GPC['sign_name'])) {
                    show_json(0, '单位名称不能空');
                }
                $data['sign_num'] = trim($_GPC['sign_num']);
			}

			$next_account = $_GPC['account_id'];
            $data['logo'] = trim($_GPC['logo']);
            $data['sign_name'] = trim($_GPC['sign_name']);
            $data['contact_name'] = trim($_GPC['contact_name']);
            $data['contact_tel'] = trim($_GPC['contact_tel']);
            $data['mobile_tel'] = trim($_GPC['mobile_tel']);
            $data['address'] = trim($_GPC['address']);
            $data['limit_time'] = trim($_GPC['limit_time']);
            $data['area'] = trim($_GPC['area']);
            $data['remark'] = trim($_GPC['remark']);
			$data['is_stop'] = intval($_GPC['is_stop']);
			if (!empty($account_id)) {
				//原关联全部去除
				pdo_update('red_account',array('parent_id'=>0),array('parent_id'=>$account_id));
				pdo_update('red_account', $data, array('account_id' => $account_id));
				//新关联绑定
				foreach ($next_account as $next){
                    pdo_update('red_account',array('parent_id'=>$account_id),array('account_id'=>$next));
				}
				plog('shop.verify.store.edit', '编辑门店 ID: ' . $account_id);
			}
			else {
				pdo_insert('red_account', $data);
				$id = pdo_insertid();
				plog('shop.verify.store.add', '添加门店 ID: ' . $id);
			}
			show_json(1, array('url' => webUrl('store')));
		}
		$item = pdo_fetch('SELECT * FROM ' . tablename('red_account') . ' WHERE is_delete = 0 and account_id = '.$account_id);
		//获取所有所属门店
		$next_account = pdo_fetchall("select sign_num,sign_name,account_id from ".tablename('red_account')." where parent_id = ".$account_id);
		include $this->template();
	}

	public function delete()
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['account_id']);
		if (empty($id)) {
			$ids = $_GPC['ids'];
            foreach ($ids as $item) {
                pdo_update('red_account', array('is_delete' =>1),array('account_id'=>$item));
            }
		}else{
            pdo_update('red_account', array('is_delete' =>1),array('account_id'=>$id));
        }
		show_json(1, array('url' => referer()));
	}

    /**
     * 合并门店搜索门店
     */
    public function query()
    {
        global $_W;
        global $_GPC;
        $kwd = trim($_GPC['keyword']);
        $pindex = max(1, intval($_GPC['page']));
        $psize = 8;
        $condition = ' parent_id = 0 and is_bind = 0 and is_delete = 0';
        if (!empty($kwd)) {
            $condition .= " AND (`sign_name` LIKE '%".$kwd."%')";
        }

        $ds = pdo_fetchall("select sign_name,sign_num,account_id,contact_tel,contact_name from ".tablename('red_account')." where ".$condition.' order by account_id desc');

        $total = pdo_fetchcolumn('SELECT COUNT(1) FROM ' . tablename('red_account') . ' WHERE 1 ' . $condition);
        $pager = pagination2($total, $pindex, $psize, '', array('before' => 5, 'after' => 4, 'ajaxcallback' => 'select_page', 'callbackfuncname' => 'select_page'));
        include $this->template();
    }

    /**
     * 创建活动时选择门店
     */
    public function queryactivity()
    {
        global $_W;
        global $_GPC;
        $kwd = trim($_GPC['keyword']);
        $pindex = max(1, intval($_GPC['page']));
        $psize = 8;
        $condition = ' parent_id = 0 and is_delete = 0';
        if (!empty($kwd)) {
            $condition .= " AND (`sign_name` LIKE '%".$kwd."%')";
        }

        $ds = pdo_fetchall("select sign_name,sign_num,account_id,contact_tel,contact_name from ".tablename('red_account')." where ".$condition.' order by account_id desc');

        $total = pdo_fetchcolumn('SELECT COUNT(1) FROM ' . tablename('red_account') . ' WHERE 1 ' . $condition);
        $pager = pagination2($total, $pindex, $psize, '', array('before' => 5, 'after' => 4, 'ajaxcallback' => 'select_page', 'callbackfuncname' => 'select_page'));
        include $this->template();
    }

    /**
     * 获取商品分类
     */
    public function category(){
       $category = pdo_fetchall("select * from ".tablename('red_category')." where parent_id = 0");
       foreach ($category as $key=>$cate){
       	 $category[$key]['next'] = pdo_fetchall("select * from ".tablename('red_category')." where parent_id = ".$cate['cat_id']);
	   }
       include $this->template();
    }

    /**
     * 获取商品品牌
     */
    public function brand(){
        $brands = pdo_fetchall("select * from ".tablename('red_brand'));
        include $this->template();
    }

    /**
     * 编辑品牌
     */
    public function brandedit(){
    	global $_W,$_GPC;
    	$brand_id = $_GPC['brand_id'];
    	if(empty($brand_id)){
    		show_json(0,'系统繁忙');
		}
    	if($_W['ispost']){
    		$brand_name = trim($_GPC['brand_name']);
    		$res = pdo_update("red_brand",array('brand_name'=>$brand_name),array('brand_id'=>$brand_id));
            if($res){
                show_json(1,'编辑成功');
            }else{
                show_json(0,'编辑失败');
            }
		}
		$brand = pdo_fetch("select * from ".tablename('red_brand')." where brand_id = ".$brand_id);
        include $this->template();
    }

    /**
     * 添加品牌
     */
    public function brandadd(){
        global $_W,$_GPC;
        if($_W['ispost']){
            $brand_name = trim($_GPC['brand_name']);
            $res = pdo_insert("red_brand",array('brand_name'=>$brand_name));
            if($res){
            	show_json(1,'添加成功');
			}else{
            	show_json(0,'添加失败');
			}
        }
        include $this->template();
	}

    /**
     * 删除品牌
     */
    public function branddelete(){
        global $_GPC;
        $brand_id = $_GPC['brand_id'];
        if(empty($brand_id)){
            show_json(0,'系统繁忙');
        }
        $res = pdo_delete('red_brand', array('brand_id' => $brand_id));
        if($res){
            show_json(1,'删除成功');
        }else{
            show_json(0,'删除失败');
        }
    }





















	public function displayorder()
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);
		$displayorder = intval($_GPC['value']);
		$item = pdo_fetchall('SELECT id,storename FROM ' . tablename('ewei_shop_store') . (' WHERE id in( ' . $id . ' ) AND uniacid=') . $_W['uniacid']);

		if (!empty($item)) {
			pdo_update('ewei_shop_store', array('displayorder' => $displayorder), array('id' => $id));
			plog('shop.verify.store.edit', '修改门店排序 ID: ' . $item['id'] . ' 门店名称: ' . $item['storename'] . ' 排序: ' . $displayorder . ' ');
		}

		show_json(1);
	}

	public function status()
	{
		global $_W;
		global $_GPC;
        $account_id = intval($_GPC['account_id']);
        $is_stop = intval($_GPC['is_stop']);

        $accountInfo = pdo_fetch("select * from ".tablename('red_account')." where account_id = ".$account_id);
        if($accountInfo){
           $res = pdo_update('red_account', array('is_stop' => $is_stop), array('account_id' => $account_id));
           if($res){
           	   show_json(1,'操作成功');
		   }else{
            	show_json(0,'操作失败');
		   }
        }else{
            show_json(0,'操作失败');
        }
	}

	public function query1()
	{
		global $_W;
		global $_GPC;
		$kwd = trim($_GPC['keyword']);
		$limittype = empty($_GPC['limittype']) ? 0 : intval($_GPC['limittype']);
		$params = array();
		$params[':uniacid'] = $_W['uniacid'];
		$condition = ' and uniacid=:uniacid  and status=1 ';

		if ($limittype == 0) {
			$condition .= '  and type in (1,2,3) ';
		}

		if (!empty($kwd)) {
			$condition .= ' AND `storename` LIKE :keyword';
			$params[':keyword'] = '%' . $kwd . '%';
		}

		$ds = pdo_fetchall('SELECT id,storename FROM ' . tablename('ewei_shop_store') . (' WHERE 1 ' . $condition . ' order by id asc'), $params);

		if ($_GPC['suggest']) {
			exit(json_encode(array('value' => $ds)));
		}

		include $this->template('shop/verify/store/query');
		exit();
	}

	public function querygoods()
	{
		global $_W;
		global $_GPC;
		$kwd = trim($_GPC['keyword']);
		$params = array();
		$params[':uniacid'] = $_W['uniacid'];
		$condition = ' and uniacid=:uniacid and deleted = 0 and `type` in (1,5,30)  and merchid =0';

		if (!empty($kwd)) {
			$condition .= ' AND `title` LIKE :keyword';
			$params[':keyword'] = '%' . $kwd . '%';
		}

		$ds = pdo_fetchall('SELECT id,title,thumb FROM ' . tablename('ewei_shop_goods') . (' WHERE 1 ' . $condition . ' order by createtime desc'), $params);
		$ds = set_medias($ds, array('thumb', 'share_icon'));

		if ($_GPC['suggest']) {
			exit(json_encode(array('value' => $ds)));
		}

		include $this->template();
	}
}

?>
