<?php
if (!defined('IN_IA')) {
	exit('Access Denied');
}

class Saler_EweiShopV2Page extends WebPage
{
	public function main()
	{
		global $_W;
		global $_GPC;

		$condition = ' where s.is_check = 1';


		if($_GPC['account_id']){
			$condition .= " and s.account_id = :account_id";
			$params[':account_id'] = $_GPC['account_id'];
		}

		if ($_GPC['status'] != '') {
			$condition .= ' and s.status = :status';
			$params[':status'] = $_GPC['status'];
		}

		if (!empty($_GPC['keyword'])) {
			$_GPC['keyword'] = trim($_GPC['keyword']);
			$condition .= ' and ( s.user_name like :keyword or m.nickname like :keyword or s.user_tel like :keyword or a.sign_name like :keyword)';
			$params[':keyword'] = '%' . $_GPC['keyword'] . '%';
		}
		$sql = " select s.m_id,m.avatar,m.nickname,s.*,a.sign_name from ".tablename('red_member')." as s left join ".tablename('ewei_shop_member')." as m on s.member_id = m.id left join ".tablename('red_account')." as a on s.account_id = a.account_id ".$condition;

		$list = pdo_fetchall($sql, $params);

		include $this->template();
	}
    public function check()
    {
        global $_W;
        global $_GPC;

        $condition = ' where s.is_check !=1';


        if($_GPC['account_id']){
            $condition .= " and s.account_id = :account_id";
            $params[':account_id'] = $_GPC['account_id'];
        }

        if ($_GPC['status'] != '') {
            $condition .= ' and s.status = :status';
            $params[':status'] = $_GPC['status'];
        }

        if (!empty($_GPC['keyword'])) {
            $_GPC['keyword'] = trim($_GPC['keyword']);
            $condition .= ' and ( s.user_name like :keyword or m.nickname like :keyword or s.user_tel like :keyword or a.sign_name like :keyword)';
            $params[':keyword'] = '%' . $_GPC['keyword'] . '%';
        }
        $sql = " select s.m_id,m.avatar,m.nickname,s.*,a.sign_name from ".tablename('red_member')." as s left join ".tablename('ewei_shop_member')." as m on s.member_id = m.id left join ".tablename('red_account')." as a on s.account_id = a.account_id ".$condition;

        $list = pdo_fetchall($sql, $params);

        include $this->template();
    }

    public function add()
	{
		$this->post();
	}

	public function edit()
	{
		$this->post();
	}
    public function checkedit()
    {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['m_id']);
        if(empty($id)){
        	show_json('系统繁忙');
		}
        if ($_W['ispost']) {
            $data = array('is_check'=>intval($_GPC['is_check']), 'account_id' => intval($_GPC['account_id']), 'status' => intval($_GPC['status']), 'user_name' => trim($_GPC['user_name']), 'user_tel' => trim($_GPC['user_tel']));
            if (empty($data['account_id'])) {
                show_json(0, '请选择所属门店');
            }
            $res = pdo_update('red_member', $data, array('m_id' => $id));
            if($res){
            	show_json(1,'审核成功');
			}else{
            	show_json('审核失败');
			}
        }
        $sql = " select s.m_id,m.avatar,m.nickname,s.*,a.sign_name,a.sign_num,a.account_id from ".tablename('red_member')." as s left join ".tablename('ewei_shop_member')." as m on s.member_id = m.id left join ".tablename('red_account')." as a on s.account_id = a.account_id  where m_id = ".$id;
        $item = pdo_fetch($sql);
        include $this->template();
    }
	protected function post()
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['m_id']);
		if ($_W['ispost']) {
			$data = array( 'account_id' => intval($_GPC['account_id']), 'member_id' => trim($_GPC['openid']), 'status' => intval($_GPC['status']), 'user_name' => trim($_GPC['user_name']), 'user_tel' => trim($_GPC['user_tel']));

			if (empty($data['account_id'])) {
				show_json(0, '请选择所属门店');
			}
            if (empty($data['member_id'])) {
                show_json(0, '请选择会员');
            }
            $scount = pdo_fetch("select * from ".tablename('red_member')." where m_id = ".$id);
			if (!empty($id)) {
				if(empty($scount)){
                    show_json(0, '编辑失败');
                }
				pdo_update('red_member', $data, array('m_id' => $id));
			}
			else {
				if (0 < $scount) {
					show_json(0, '此会员已经成为店员，没法重复添加');
				}
				$res = pdo_fetch("select * from ".tablename('red_member')." where member_id = ".$data['member_id']." and is_check = 1");
			    if($res>0){
                    show_json(0, '此会员已成为其他门店的店员了，没法选择');
                }
				$data['add_time'] = time();
                $data['is_check'] = 1;

                pdo_insert('red_member', $data);
			}
			show_json(1, array('url' => webUrl('store/saler')));
		}
        $sql = " select s.m_id,m.avatar,m.nickname,s.*,a.sign_name,a.sign_num,a.account_id from ".tablename('red_member')." as s left join ".tablename('ewei_shop_member')." as m on s.member_id = m.id left join ".tablename('red_account')." as a on s.account_id = a.account_id  where m_id = ".$id;
        $item = pdo_fetch($sql);
	    include $this->template();
	}

    /**
     * 删除门店和店员的绑定关系
     */
    public function deletebindmember(){
        global $_GPC,$_W;
        $ids = array($_GPC['ids']);
        if(empty($ids)){
            show_json(0,'系统繁忙');
        }
        foreach ($ids as $m_id){
            pdo_delete('red_member',array('m_id'=>$m_id));
        }
        show_json(1,'删除成功');
	}
	public function query(){
        global $_W;
        global $_GPC;
        $kwd = trim($_GPC['keyword']);
        $pindex = max(1, intval($_GPC['page']));
        $psize = 8;
        $condition = ' parent_id = 0 and is_delete = 0';
        if (!empty($kwd)) {
            $condition .= " AND (`sign_name` LIKE '%".$kwd."%' or sign_num like '%".$kwd."%')";
        }

        $ds = pdo_fetchall("select sign_name,sign_num,account_id,contact_tel,contact_name from ".tablename('red_account')." where ".$condition.' order by account_id desc');

        $total = pdo_fetchcolumn('SELECT COUNT(1) FROM ' . tablename('red_account') . ' WHERE 1 ' . $condition);
        $pager = pagination2($total, $pindex, $psize, '', array('before' => 5, 'after' => 4, 'ajaxcallback' => 'select_page', 'callbackfuncname' => 'select_page'));
        include $this->template();
	}

	public function delete()
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);

		if (empty($id)) {
			$id = is_array($_GPC['ids']) ? implode(',', $_GPC['ids']) : 0;
		}

		$items = pdo_fetchall('SELECT id,salername FROM ' . tablename('ewei_shop_saler') . (' WHERE id in( ' . $id . ' ) AND uniacid=') . $_W['uniacid']);

		foreach ($items as $item) {
			pdo_delete('ewei_shop_saler', array('id' => $item['id']));
			plog('shop.verify.saler.delete', '删除店员 ID: ' . $item['id'] . ' 店员名称: ' . $item['salername'] . ' ');
		}

		show_json(1, array('url' => referer()));
	}

	public function status()
	{
		global $_W;
		global $_GPC;
		$ids = array($_GPC['ids']);
		if(empty($ids)){
			show_json(0,'系统繁忙');
		}
		foreach ($ids as $m_id){
            pdo_update('red_member', array('status' => intval($_GPC['status'])), array('m_id' => $m_id));
        }
		show_json(1, array('url' => referer()));
	}

//	public function query()
//	{
//		global $_W;
//		global $_GPC;
//		$kwd = trim($_GPC['keyword']);
//		$params = array();
//		$params[':uniacid'] = $_W['uniacid'];
//		$condition = ' and s.uniacid=:uniacid';
//
//		if (!empty($kwd)) {
//			$condition .= ' AND ( m.nickname LIKE :keyword or m.realname LIKE :keyword or m.mobile LIKE :keyword or store.storename like :keyword )';
//			$params[':keyword'] = '%' . $kwd . '%';
//		}
//
//		$ds = pdo_fetchall('SELECT s.*,m.nickname,m.avatar,m.mobile,m.realname,store.storename FROM ' . tablename('ewei_shop_saler') . '  s ' . ' left join ' . tablename('ewei_shop_member') . ' m on s.openid=m.openid ' . ' left join ' . tablename('ewei_shop_store') . ' store on store.id=s.storeid ' . (' WHERE 1 ' . $condition . ' ORDER BY id asc'), $params);
//		include $this->template();
//		exit();
//	}
}

?>
