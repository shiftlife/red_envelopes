<?php
if (!defined('IN_IA')) {
	exit('Access Denied');
}

class Category_EweiShopV2Page extends WebPage
{
	public function main()
	{
		global $_W;
		global $_GPC;

		if ($_W['ispost']) {
			if (!empty($_GPC['datas'])) {
				$datas = json_decode(html_entity_decode($_GPC['datas']), true);

				if (!is_array($datas)) {
					show_json(0, '分类保存失败，请重试!');
				}

				$cateids = array();
				$displayorder = count($datas);

				foreach ($datas as $row) {
					$cateids[] = $row['id'];
					pdo_update('ewei_shop_category', array('parentid' => 0, 'displayorder' => $displayorder, 'level' => 1), array('id' => $row['id']));
					if ($row['children'] && is_array($row['children'])) {
						$displayorder_child = count($row['children']);

						foreach ($row['children'] as $child) {
							$cateids[] = $child['id'];
							pdo_query('update ' . tablename('ewei_shop_category') . ' set  parentid=:parentid,displayorder=:displayorder,level=2 where id=:id', array(':displayorder' => $displayorder_child, ':parentid' => $row['id'], ':id' => $child['id']));
							--$displayorder_child;
							if ($child['children'] && is_array($child['children'])) {
								$displayorder_third = count($child['children']);

								foreach ($child['children'] as $third) {
									$cateids[] = $third['id'];
									pdo_query('update ' . tablename('ewei_shop_category') . ' set  parentid=:parentid,displayorder=:displayorder,level=3 where id=:id', array(':displayorder' => $displayorder_third, ':parentid' => $child['id'], ':id' => $third['id']));
									--$displayorder_third;
									if ($third['children'] && is_array($third['children'])) {
										$displayorder_fourth = count($third['children']);

										foreach ($child['children'] as $fourth) {
											$cateids[] = $fourth['id'];
											pdo_query('update ' . tablename('ewei_shop_category') . ' set  parentid=:parentid,displayorder=:displayorder,level=3 where id=:id', array(':displayorder' => $displayorder_third, ':parentid' => $third['id'], ':id' => $fourth['id']));
											--$displayorder_fourth;
										}
									}
								}
							}
						}
					}

					--$displayorder;
				}

				if (!empty($cateids)) {
					pdo_query('delete from ' . tablename('ewei_shop_category') . ' where id not in (' . implode(',', $cateids) . ') and uniacid=:uniacid', array(':uniacid' => $_W['uniacid']));
				}

				plog('shop.category.edit', '批量修改分类的层级及排序');
				m('shop')->getCategory(true);
				m('shop')->getAllCategory(true);
				show_json(1);
			}
		}

		$children = array();
		$category = pdo_fetchall('SELECT * FROM ' . tablename('ewei_shop_category') . (' WHERE uniacid = \'' . $_W['uniacid'] . '\' ORDER BY parentid ASC, displayorder DESC'));

		foreach ($category as $index => $row) {
			if (!empty($row['parentid'])) {
				$children[$row['parentid']][] = $row;
				unset($category[$index]);
			}
		}

		include $this->template();
	}

	public function add()
	{
		$this->post();
	}

	public function edit()
	{
		$this->post();
	}

	protected function post()
	{
		global $_W;
		global $_GPC;
		$cat_id = intval($_GPC['cat_id']);
        $parent_id = intval($_GPC['parent_id']);

        if($_W['ispost']){
            $type_name = $_GPC['type_name'];
            if(empty($type_name)){
                show_json(0,'分类名称不能为空');
            }
            if(empty($cat_id)){
                //添加
               $res = pdo_insert('red_category',array('type_name'=>$type_name,'parent_id'=>$parent_id));
            }else{
            	$res = pdo_update('red_category',array('type_name'=>$type_name),array('cat_id'=>$cat_id));
            }
            if($res){
            	show_json(1,'操作成功');
			}else{
            	show_json(0,'操作失败');
			}
        }
        $catInfo = pdo_fetch("select * from ".tablename('red_category')." where cat_id = ".$cat_id);
		include $this->template();
	}

	public function delete()
	{
		global $_W;
		global $_GPC;
		$cat_id = intval($_GPC['cat_id']);
		if(empty($cat_id)){
			show_json(0,'系统繁忙');
		}
		$item = pdo_fetch('SELECT * FROM ' . tablename('red_category') ." where cat_id = ".$cat_id );
		if (empty($item)) {
			$this->message('抱歉，分类不存在或是已经被删除！', webUrl('goods/category', array('op' => 'display')), 'error');
		}

		if($item['parent_id'] == 0){
			$res = pdo_fetch("select * from ".tablename('red_category')." where parent_id = ".$item['cat_id']);
			if($res){
				show_json(0,'必须先删除子类');
			}
		}
		pdo_delete('red_category', array('cat_id' => $cat_id));
		show_json(1, array('url' => referer()));
	}

	public function enabled()
	{
		global $_W;
		global $_GPC;
		$id = intval($_GPC['id']);

		if (empty($id)) {
			$id = is_array($_GPC['ids']) ? implode(',', $_GPC['ids']) : 0;
		}

		$items = pdo_fetchall('SELECT id,name FROM ' . tablename('ewei_shop_category') . (' WHERE id in( ' . $id . ' ) AND uniacid=') . $_W['uniacid']);

		foreach ($items as $item) {
			pdo_update('ewei_shop_category', array('enabled' => intval($_GPC['enabled'])), array('id' => $item['id']));
			plog('shop.dispatch.edit', '修改分类状态<br/>ID: ' . $item['id'] . '<br/>分类名称: ' . $item['name'] . '<br/>状态: ' . $_GPC['enabled'] == 1 ? '显示' : '隐藏');
		}

		m('shop')->getCategory(true);
		show_json(1, array('url' => referer()));
	}

	public function query()
	{
		global $_W;
		global $_GPC;
		$kwd = trim($_GPC['keyword']);
		$params = array();
		$params[':uniacid'] = $_W['uniacid'];
		$condition = ' and enabled=1 and uniacid=:uniacid';

		if (!empty($kwd)) {
			$condition .= ' AND `name` LIKE :keyword';
			$params[':keyword'] = '%' . $kwd . '%';
		}

		$ds = pdo_fetchall('SELECT * FROM ' . tablename('ewei_shop_category') . (' WHERE 1 ' . $condition . ' order by displayorder desc,id desc'), $params);
		$ds = set_medias($ds, array('thumb', 'advimg'));

		if ($_GPC['suggest']) {
			exit(json_encode(array('value' => $ds)));
		}

		include $this->template();
	}
}

?>
