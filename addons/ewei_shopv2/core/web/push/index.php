<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}

class Index_EweiShopV2Page extends WebPage
{
    /**
     * 获取所有的活动
     */
    public function main()
    {
        global $_GPC, $_W;
        $account_id = intval($_GPC['account_id']);
        $where = ' and 1';
        if ($account_id) {
            $where .= " and c.account_id = " . $account_id;
        }
        $keyword = trim($_GPC['keyword']);
        if ($keyword) {
            $where .= " and a.active_name like '%" . $keyword . "%'";
        }
        if (!empty($_GPC['time']['start']) && !empty($_GPC['time']['end'])) {
            $starttime = strtotime($_GPC['time']['start']);
            $endtime = strtotime($_GPC['time']['end']);
            $where .= ' AND a.start_time >= ' . $starttime . ' AND a.end_time <= ' . $endtime;
        }
        $activity = pdo_fetchall("select * from " . tablename('red_activity') . " as a left join " . tablename('red_active_account') . " as c on a.activity_id = c.activity_id  where a.is_delete = 0 and a.is_hide = 0" . $where . " group by a.activity_id  order by a.sort desc");

        $today = strtotime(date('Y-m-d', time()));
        foreach ($activity as $key => $active) {
            if ($today >= $active['start_time'] && $today <= $active['end_time']) {
                $activity[$key]['status'] = '进行中';
            } elseif ($today < $active['start_time']) {
                $activity[$key]['status'] = '未开始';
            } else {
                $activity[$key]['status'] = '已结束';
            }
            $goods = pdo_fetchall("select * from " . tablename('red_active_goods') . " where activity_id = " . $active['activity_id']);
            $activity[$key]['goods_num'] = count($goods);
            $account = pdo_fetchall("select * from " . tablename('red_active_account') . "where activity_id = " . $active['activity_id']);
            $activity[$key]['account_num'] = count($account);
        }
        include $this->template();
    }

    /**
     * 添加活动
     */
    public function addactive()
    {
        global $_GPC, $_W;
        $activity_id = $_GPC['activity_id'];
        if ($_W['ispost']) {
            $sort = $_GPC['sort'];
            $active_name = $_GPC['active_name'];
            $thumb = $_GPC['thumb'];
            $start_time = $_GPC['time']['start'];
            $end_time = $_GPC['time']['end'];
            $account_ids = $_GPC['account_id'];
            $goods_ids = $_GPC['goods_id'];
            $prices = $_GPC['price'];
            $remark = $_GPC['remark'];
            if (empty($active_name)) {
                show_json(0, '活动名称不能为空');
            }
            if (empty($_GPC['time'])) {
                show_json(0, '活动时间不能为空');
            }
            if (empty($account_ids)) {
                show_json(0, '必须选择门店');
            }
            if (empty($goods_ids)) {
                show_json(0, '必须选择商品');
            }
            $data = array('remark' => $remark, 'thumb' => $thumb, 'active_name' => $active_name, 'sort' => $sort, 'start_time' => strtotime($start_time), 'end_time' => strtotime($end_time), 'add_time' => time());
            pdo_insert('red_activity', $data);
            $activity_id = pdo_insertid();
            if ($activity_id) {
                foreach ($goods_ids as $key => $goods_id) {
                    $price = $prices[$key];
                    pdo_insert('red_active_goods', array('goods_id' => $goods_id, 'price' => $price, 'activity_id' => $activity_id));
                }
                foreach ($account_ids as $account_id) {
                    pdo_insert('red_active_account', array('account_id' => $account_id, 'activity_id' => $activity_id));
                }
                show_json(1, '添加成功');
            } else {
                show_json(0, '添加失败');
            }
        }
        $activity = pdo_fetch("select * from " . tablename('red_activity') . " where activity_id = " . $activity_id);
        $goods = pdo_fetchall("select * from " . tablename('red_active_goods') . " where activity_id = " . $activity_id);
        $account = pdo_fetchall("select * from " . tablename('red_active_account') . " where activity_id=" . $activity_id);
        $ds = array();
        $item = array();
        foreach ($account as $value) {
            $ds[] = pdo_fetch("select * from " . tablename('red_account') . " where account_id = " . $value['account_id']);
        }
        foreach ($goods as $key => $value) {
            $res = pdo_fetch("select *,price as goods_price from " . tablename('red_goods') . " where goods_id = " . $value['goods_id']);
            $item[] = array_merge($res, $value);
        }
        $level_array = array();
        $i = 0;
        while ($i < 101) {
            $level_array[$i] = $i;
            ++$i;
        }
        include $this->template();
    }

    /**
     * 修改活动
     */
    public function active_detail()
    {
        global $_GPC, $_W;
        $activity_id = $_GPC['activity_id'];
        $activity = pdo_fetch("select * from " . tablename('red_activity') . " where activity_id = " . $activity_id);
        $goods = pdo_fetchall("select * from " . tablename('red_active_goods') . " where activity_id = " . $activity_id);
        $account = pdo_fetchall("select * from " . tablename('red_active_account') . " where activity_id=" . $activity_id);
        $ds = array();
        $item = array();
        foreach ($account as $value) {
            $ds[] = pdo_fetch("select * from " . tablename('red_account') . " where account_id = " . $value['account_id']);
            $account_arr[] = $value['account_id'];
        }
        foreach ($goods as $key => $value) {
            $res = pdo_fetch("select *,price as goods_price from " . tablename('red_goods') . " where goods_id = " . $value['goods_id']);
            $item[] = array_merge($res, $value);
            $goods_arr[] = $value['goods_id'];
        }
        $level_array = array();
        $i = 0;
        while ($i < 101) {
            $level_array[$i] = $i;
            ++$i;
        }

        if ($_W['ispost']) {
            $sort = $_GPC['sort'];
            $active_name = $_GPC['active_name'];
            $thumb = $_GPC['thumb'];
            $start_time = $_GPC['time']['start'];
            $end_time = $_GPC['time']['end'];
            $remark = $_GPC['remark'];

            //判断是否修改门店和商品
            $account_ids = $_GPC['account_id'];
            $account_diff1 = array_diff($account_arr, $account_ids);
            $account_diff2 = array_diff($account_ids, $account_arr);
            $goods_ids = $_GPC['goods_id'];
            $goods_diff1 = array_diff($goods_arr, $goods_ids);
            $goods_diff2 = array_diff($goods_ids, $goods_arr);

            //判断是否修改价格
            $prices = $_GPC['price'];
            foreach ($goods_ids as $key => $goods_id) {
                $price = $prices[$goods_id];
                $goods_price = pdo_fetchcolumn('select price from ' . tablename('red_active_goods') . ' where goods_id = ' . $goods_id . ' and activity_id = ' . $activity_id);
                if ($price != $goods_price) {
                    $priceischange = 1;
                    break;
                }
            }

            if (empty($active_name)) {
                show_json(0, '活动名称不能为空');
            }
            if (empty($_GPC['time'])) {
                show_json(0, '活动时间不能为空');
            }
            if (empty($account_ids)) {
                show_json(0, '必须选择门店');
            }
            if (empty($goods_ids)) {
                show_json(0, '必须选择商品');
            }
            $data = array(
                'remark' => $remark,
                'thumb' => $thumb,
                'active_name' => $active_name,
                'sort' => $sort,
                'start_time' => strtotime($start_time),
                'end_time' => strtotime($end_time),
                'add_time' => time()
            );
            //如果只有时间标题等改变则修改
            if (count($account_diff1) == 0 && count($account_diff2) == 0 && count($goods_diff1) == 0 && count($goods_diff2) == 0 && $priceischange != 1) {
                pdo_update('red_activity', $data, array('activity_id' => $activity_id));
                show_json(1, '修改成功');
            }
            //如果有专卖店或商品更改则新建活动
            pdo_update('red_activity', array('is_hide'=>1,'end_time' => strtotime(date("Y-m-d 23:59:59", strtotime("-1 day")))), array('activity_id' => $activity_id));
            if($activity['parent_activity_id']){
                $data['parent_activity_id'] = $activity['parent_activity_id'];
            }else{
                $data['parent_activity_id'] = $activity_id;
            }
            pdo_insert('red_activity', $data);
            $activity_id = pdo_insertid();
            if ($activity_id) {
                foreach ($goods_ids as $key => $goods_id) {
                    $price = $prices[$goods_id];
                    pdo_insert('red_active_goods', array('goods_id' => $goods_id, 'price' => $price, 'activity_id' => $activity_id));
                }
                foreach ($account_ids as $account_id) {
                    pdo_insert('red_active_account', array('account_id' => $account_id, 'activity_id' => $activity_id));
                }
                show_json(1, array('message' => '添加成功', 'url' => webUrl('push/active_detail', array('activity_id' => $activity_id))));
            } else {
                show_json(0, '添加失败');
            }
        }
        include $this->template();
    }

    /**
     * 获取商品
     */
    public function querygoods()
    {
        global $_GPC;
        $keyword = $_GPC['keyword'];
        $condition = '';
        if ($keyword) {
            $condition = " and (goods_name like '%" . $keyword . "%' or bar_code like '%" . $keyword . "%')";
        }
        $ds = pdo_fetchall("select * from " . tablename('red_goods') . " where is_delete = 0" . $condition);
        include $this->template();
    }

    /*
     * 删除活动
     */
    public function delactivity()
    {
        global $_GPC;
        $activity_id = $_GPC['activity_id'];
        $data = pdo_fetch("select * from " . tablename('red_activity') . " where activity_id = " . $activity_id);
        if ($data) {
            $res = pdo_update('red_activity', array('is_delete' => 1), array('activity_id' => $activity_id));
            if ($res) {
                show_json(1, '删除成功');
            } else {
                show_json(0, '删除失败');
            }
        } else {
            show_json(0, '删除失败');
        }
    }
}

?>
