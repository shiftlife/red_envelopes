<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}

class Log_EweiShopV2Page extends WebPage
{
    protected function main($type = 0)
    {
        $this->orderlist();

    }

    public function refund($tid = 0, $fee = 0, $reason = '')
    {
        global $_W;
        global $_GPC;
        $set = $_W['shopset']['shop'];
        $id = intval($_GPC['id']);
        $log = pdo_fetch('select * from ' . tablename('ewei_shop_member_log') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));

        if (empty($log)) {
            show_json(0, '未找到记录!');
        }

        if (!empty($log['type'])) {
            show_json(0, '非充值记录!');
        }

        if ($log['rechargetype'] == 'system') {
            show_json(0, '后台充值无法退款!');
        }

        $current_credit = m('member')->getCredit($log['openid'], 'credit2');

        if ($current_credit < $log['money']) {
            show_json(0, '会员账户余额不足，无法进行退款!');
        }

        $out_refund_no = 'RR' . substr($log['logno'], 2);

        if ($log['rechargetype'] == 'wechat') {
            if ($log['apppay'] == 2) {
                $result = m('finance')->wxapp_refund($log['openid'], $log['logno'], $out_refund_no, $log['money'] * 100, $log['money'] * 100, !empty($log['apppay']) ? true : false);
            } else if (empty($log['isborrow'])) {
                $result = m('finance')->refund($log['openid'], $log['logno'], $out_refund_no, $log['money'] * 100, $log['money'] * 100, !empty($log['apppay']) ? true : false);
            } else {
                $result = m('finance')->refundBorrow($log['openid'], $log['logno'], $out_refund_no, $log['money'] * 100, $log['money'] * 100);
            }
        } else if ($log['rechargetype'] == 'alipay') {
            $sec = m('common')->getSec();
            $sec = iunserializer($sec['sec']);

            if (!empty($log['apppay'])) {
                if (!empty($sec['app_alipay']['private_key_rsa2'])) {
                    $sign_type = 'RSA2';
                    $privatekey = $sec['app_alipay']['private_key_rsa2'];
                } else {
                    $sign_type = 'RSA';
                    $privatekey = $sec['app_alipay']['private_key'];
                }

                if (empty($privatekey) || empty($sec['app_alipay']['appid'])) {
                    show_json(0, '支付参数错误，私钥为空或者APPID为空!');
                }

                $params = array('out_trade_no' => $log['logno'], 'refund_amount' => $log['money'], 'refund_reason' => '会员充值退款: ' . $log['money'] . '元 订单号: ' . $log['logno'] . '/' . $out_refund_no);
                $config = array('app_id' => $sec['app_alipay']['appid'], 'privatekey' => $privatekey, 'publickey' => '', 'alipublickey' => '', 'sign_type' => $sign_type);
                $result = m('finance')->newAlipayRefund($params, $config);
            } else if (!empty($sec['alipay_pay'])) {
                if (empty($sec['alipay_pay']['private_key']) || empty($sec['alipay_pay']['appid'])) {
                    show_json(0, '支付参数错误，私钥为空或者APPID为空!');
                }

                if ($sec['alipay_pay']['alipay_sign_type'] == 1) {
                    $sign_type = 'RSA2';
                } else {
                    $sign_type = 'RSA';
                }

                $params = array('out_request_no' => time(), 'out_trade_no' => $log['logno'], 'refund_amount' => $log['money'], 'refund_reason' => '会员充值退款: ' . $log['money'] . '元 订单号: ' . $log['logno'] . '/' . $out_refund_no);
                $config = array('app_id' => $sec['alipay_pay']['appid'], 'privatekey' => $sec['alipay_pay']['private_key'], 'publickey' => '', 'alipublickey' => '', 'sign_type' => $sign_type);
                $result = m('finance')->newAlipayRefund($params, $config);
            } else {
                if (empty($log['transid'])) {
                    show_json(0, '仅支持 升级后此功能后退款的订单!');
                }

                $setting = uni_setting($_W['uniacid'], array('payment'));

                if (!is_array($setting['payment'])) {
                    return error(1, '没有设定支付参数');
                }

                $alipay_config = $setting['payment']['alipay'];
                $batch_no_money = $log['money'] * 100;
                $batch_no = date('Ymd') . 'RC' . $log['id'] . 'MONEY' . $batch_no_money;
                $res = m('finance')->AlipayRefund(array('trade_no' => $log['transid'], 'refund_price' => $log['money'], 'refund_reason' => '会员充值退款: ' . $log['money'] . '元 订单号: ' . $log['logno'] . '/' . $out_refund_no), $batch_no, $alipay_config);

                if (is_error($res)) {
                    show_json(0, $res['message']);
                }

                show_json(1, array('url' => $res));
            }
        } else {
            $result = m('finance')->pay($log['openid'], 1, $log['money'] * 100, $out_refund_no, $set['name'] . '充值退款');
        }

        if (is_error($result)) {
            show_json(0, $result['message']);
        }

        pdo_update('ewei_shop_member_log', array('status' => 3), array('id' => $id, 'uniacid' => $_W['uniacid']));
        $refundmoney = $log['money'] + $log['gives'];
        m('member')->setCredit($log['openid'], 'credit2', 0 - $refundmoney, array(0, $set['name'] . '充值退款'));
        $money = com_run('sale::getCredit1', $log['openid'], (double)$log['money'], 21, 2, 1);

        if (0 < $money) {
            m('notice')->sendMemberPointChange($log['openid'], $money, 1);
        }

        m('notice')->sendMemberLogMessage($log['id']);
        $member = m('member')->getMember($log['openid']);
        plog('finance.log.refund', '充值退款 ID: ' . $log['id'] . ' 金额: ' . $log['money'] . ' <br/>会员信息:  ID: ' . $member['id'] . ' / ' . $member['openid'] . '/' . $member['nickname'] . '/' . $member['realname'] . '/' . $member['mobile']);
        show_json(1, array('url' => referer()));
    }

    public function wechat()
    {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['id']);
        $log = pdo_fetch('select * from ' . tablename('ewei_shop_member_log') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));

        if (empty($log)) {
            show_json(0, '未找到记录!');
        }

        if ($log['deductionmoney'] == 0) {
            $realmoney = $log['money'];
        } else {
            $realmoney = $log['realmoney'];
        }

        $set = $_W['shopset']['shop'];
        $data = m('common')->getSysset('pay');

        if (!empty($data['paytype']['withdraw'])) {
            $result = m('finance')->payRedPack($log['openid'], $realmoney * 100, $log['logno'], $log, $set['name'] . '余额提现', $data['paytype']);
            pdo_update('ewei_shop_member_log', array('sendmoney' => $result['sendmoney'], 'senddata' => json_encode($result['senddata'])), array('id' => $log['id']));

            if ($result['sendmoney'] == $realmoney) {
                $result = true;
            } else {
                $result = $result['error'];
            }
        } else {
            $result = m('finance')->pay($log['openid'], 1, $realmoney * 100, $log['logno'], $set['name'] . '余额提现');
        }

        if (is_error($result)) {
            show_json(0, array('message' => $result['message']));
        }

        pdo_update('ewei_shop_member_log', array('status' => 1), array('id' => $id, 'uniacid' => $_W['uniacid']));
        m('notice')->sendMemberLogMessage($log['id']);
        $member = m('member')->getMember($log['openid']);
        plog('finance.log.wechat', '余额提现 ID: ' . $log['id'] . ' 方式: 微信 提现金额: ' . $log['money'] . ' ,到账金额: ' . $realmoney . ' ,手续费金额 : ' . $log['deductionmoney'] . '<br/>会员信息:  ID: ' . $member['id'] . ' / ' . $member['openid'] . '/' . $member['nickname'] . '/' . $member['realname'] . '/' . $member['mobile']);
        show_json(1);
    }

    public function alipay()
    {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['id']);
        $log = pdo_fetch('select * from ' . tablename('ewei_shop_member_log') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));

        if (empty($log)) {
            show_json(0, '未找到记录!');
        }

        if ($log['deductionmoney'] == 0) {
            $realmoeny = $log['money'];
        } else {
            $realmoeny = $log['realmoney'];
        }

        $set = $_W['shopset']['shop'];
        $sec = m('common')->getSec();
        $sec = iunserializer($sec['sec']);

        if (!empty($sec['alipay_pay']['open'])) {
            $batch_no_money = $realmoeny * 100;
            $batch_no = 'D' . date('Ymd') . 'RW' . $log['id'] . 'MONEY' . $batch_no_money;
            $res = m('finance')->AliPay(array('account' => $log['alipay'], 'name' => $log['realname'], 'money' => $realmoeny), $batch_no, $sec['alipay_pay'], $log['title']);

            if (is_error($res)) {
                show_json(0, $res['message']);
            }

            show_json(1, array('url' => $res));
        }

        show_json(0, '未开启,支付宝打款!');
    }

    public function manual()
    {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['id']);
        $log = pdo_fetch('select * from ' . tablename('ewei_shop_member_log') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));

        if (empty($log)) {
            show_json(0, '未找到记录!');
        }

        $member = m('member')->getMember($log['openid']);
        pdo_update('ewei_shop_member_log', array('status' => 1), array('id' => $id, 'uniacid' => $_W['uniacid']));
        m('notice')->sendMemberLogMessage($log['id']);
        plog('finance.log.manual', '余额提现 方式: 手动 ID: ' . $log['id'] . ' <br/>会员信息: ID: ' . $member['id'] . ' / ' . $member['openid'] . '/' . $member['nickname'] . '/' . $member['realname'] . '/' . $member['mobile']);
        show_json(1);
    }

    public function refuse()
    {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['id']);
        $log = pdo_fetch('select * from ' . tablename('ewei_shop_member_log') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));

        if (empty($log)) {
            show_json(0, '未找到记录!');
        }

        if ($log['status'] == -1) {
            show_json(0, '退款申请已经处理!');
        }

        pdo_update('ewei_shop_member_log', array('status' => -1), array('id' => $id, 'uniacid' => $_W['uniacid']));

        if (0 < $log['money']) {
            m('member')->setCredit($log['openid'], 'credit2', $log['money'], array(0, '余额提现退回'));
        }

        $member = pdo_fetchall('SELECT * FROM ' . tablename('ewei_shop_member') . ' WHERE uniacid =:uniacid AND openid=:openid', array(':uniacid' => $_W['uniacid'], ':openid' => $log['openid']));
        m('notice')->sendMemberLogMessage($log['id']);
        plog('finance.log.refuse', '拒绝余额度提现 ID: ' . $log['id'] . ' 金额: ' . $log['money'] . ' <br/>会员信息:  ID: ' . $member['id'] . ' / ' . $member['openid'] . '/' . $member['nickname'] . '/' . $member['realname'] . '/' . $member['mobile']);
        show_json(1);
    }

    public function recharge()
    {
        $this->main(0);
    }

    public function withdraw()
    {
        $this->main(1);
    }

    /**
     * 提单列表
     */
    public function orderlist()
    {
        global $_GPC, $_W;
        $pindex = max(1, intval($_GPC['page']));
        $psize = 20;
        $where = 'd.is_delete = 0 and o.user_id >0 ';
        if (!empty($_GPC['time']['start']) && !empty($_GPC['time']['end'])) {
            $starttime = strtotime($_GPC['time']['start']);
            $endtime = strtotime($_GPC['time']['end']);
            $where .= ' AND o.add_time >= ' . $starttime . ' AND o.add_time <= ' . $endtime;
        }
        if ($_GPC['searchfield']) {
            if ($_GPC['searchfield'] == 1) {
                $new_str = trim($_GPC['keyword']);
                $where .= " and o.order_num like '%" . $new_str . "%'";
            } elseif ($_GPC['searchfield'] == 2) {
                $where .= " and m.realname like '%" . trim($_GPC['keyword']) . "%'";
            }
        }
        if ($_GPC['status'] != null && $_GPC['status'] != 'all') {
            $status = $_GPC['status'];
            if ($status == 3) {
                $status = 0;
            }
            $where .= "  and d.status = " . $status;
        }
        $order_list = pdo_fetchall("select d.*,o.order_num,o.add_time,m.realname,m.mobile,g.goods_name,g.bar_code,g.thumb,o.sign_name from " . tablename('red_order_detail') . " as d left join " . tablename('red_order') . " as o on d.order_id = o.order_id left join " . tablename('ewei_shop_member') . "  as m on o.user_id = m.id left join " . tablename('red_goods') . " as g on g.goods_id = d.goods_id left join " . tablename('red_account') . " a on a.account_id = m.account_id where " . $where . " order by o.order_id desc " . 'LIMIT ' . (($pindex - 1) * $psize) . ',' . $psize);
        $total = pdo_fetchcolumn("select count(*) from " . tablename('red_order_detail') . " as d left join " . tablename('red_order') . " as o on d.order_id = o.order_id left join " . tablename('ewei_shop_member') . "  as m on o.user_id = m.id left join " . tablename('red_goods') . " as g on g.goods_id = d.goods_id  where " . $where);
        $pager = pagination2($total, $pindex, $psize);
        foreach ($order_list as $key => $order) {
            $order_list[$key]['order_num'] = $order['order_num'] . '-' . $order['num_id'];
        }
        if ($_GPC['export'] == 1) {

            $list = pdo_fetchall("select d.*,o.order_num,o.add_time,o.sale_time,o.pic,m.realname,m.mobile,g.goods_name,g.bar_code,o.sign_name from " . tablename('red_order_detail') . " as d left join " . tablename('red_order') . " as o on d.order_id = o.order_id left join " . tablename('ewei_shop_member') . "  as m on o.user_id = m.id left join " . tablename('red_goods') . " as g on g.goods_id = d.goods_id left join " . tablename('red_account') . " a on a.account_id = m.account_id  where " . $where . " order by o.order_id desc ");

            $columns = array(array('title' => '产品', 'field' => 'product', 'width' => 50), array('title' => '产品条码', 'field' => 'code', 'width' => 20), array('title' => '门店', 'field' => 'account', 'width' => 35), array('title' => '提单人', 'field' => 'realname', 'width' => 15), array('title' => '手机号', 'field' => 'mobile', 'width' => 15), array('title' => '数量', 'field' => 'amount', 'width' => 10), array('title' => '单品佣金', 'field' => 'single_price', 'width' => 10), array('title' => '总额', 'field' => 'price', 'width' => 10), array('title' => '提单时间', 'field' => 'add_time', 'width' => 20), array('title' => '销售时间', 'field' => 'sale_time', 'width' => 20), array('title' => '状态', 'field' => 'status', 'width' => 15), array('title' => '小票图片', 'field' => 'pic', 'width' => 50));
            $exportlist = array();
            foreach ($list as $key => $value) {
                if ($value['status'] == 0) {
                    $status = '待审核';
                }
                if ($value['status'] == 1) {
                    $status = '审核通过';
                }
                if ($value['status'] == 2) {
                    $status = '审核拒绝';
                }
                $pic = rtrim($value['pic'], ';');
                $pic = explode(';', $pic);
                foreach ($pic as &$p) {
                    $p = substr($p, 2);
                    $p = 'https://kf.wth123.cn' . $p;
                }
                $pic = implode(';', $pic);

                $r['product'] = $value['goods_name'];
                $r['code'] = '\'' . $value['bar_code'];
                $r['account'] = $value['sign_name'];
                $r['realname'] = $value['realname'];
                $r['mobile'] = $value['mobile'];
                $r['amount'] = $value['amount'];
                $r['single_price'] = $value['single_price'];
                $r['price'] = $value['price'];
                $r['sale_time'] = date('Y-m-d', $value['sale_time']);
                $r['add_time'] = date('Y-m-d h:i:s', $value['add_time']);
                $r['status'] = $status;
                $r['pic'] = $pic;

                $exportlist[] = $r;
            }
            unset($r);
            m('excel')->export($exportlist, array('title' => '提单数据', 'columns' => $columns));
        }
        include $this->template('finance/log/orderlist');

    }

    /**
     * 提现审批
     */
    public function ordercheck()
    {
        global $_GPC, $_W;
        $id = $_GPC['id'];
        load()->func('tpl');
        if (empty($id)) {
            show_json(0, '系统繁忙');
        }
        $order = pdo_fetch("select d.*,o.order_num,o.add_time,o.user_id,o.pic,m.realname,m.mobile,g.goods_name,g.bar_code,a.sign_name,m.mobile,m.account_id,g.thumb,o.account_id as oaccount_id from " . tablename('red_order_detail') . " as d left join " . tablename('red_order') . " as o on d.order_id = o.order_id left join " . tablename('ewei_shop_member') . "  as m on o.user_id = m.id left join " . tablename('red_goods') . " as g on g.goods_id = d.goods_id left join " . tablename('red_account') . " a on m.account_id = a.account_id where d.is_delete = 0 and o.user_id > 0 and d.id = " . $id);
        $order_log = pdo_fetchall('select * from ' . tablename('red_order_detail_log') . ' where order_detail_id = :id order by createtime desc', array(':id' => $id));
        $order_pic = explode(';', $order['pic']);
        $goods = pdo_fetchall('select * from ' . tablename('red_detail') . ' d left join ' . tablename('red_goods') . ' g on d.goods_id = g.goods_id where d.account_id = :account_id', array(':account_id' => $order['account_id']));
        $xiaopiao = pdo_fetchall('select * from ' . tablename('red_order') . ' where user_id = :user_id order by add_time desc limit 5 ', array(':user_id' => $order['user_id']));
        $xiaopiao_str = '';
        foreach ($xiaopiao as $xp) {
            $xiaopiao_str .= $xp['pic'];
        }
        $xiaopiao_arr = explode(';', $xiaopiao_str);
        if ($order) {
            $order['order_num'] = $order['order_num'] . '-' . $order['num_id'];
            $order['pic'] = array_filter(explode(';', $order['pic']));
            if ($_W['ispost']) {
                $remark = $_GPC['remark'];
                $status = intval($_GPC['status']);
                if (empty($status)) {
                    show_json(0, '请选择审核状态');
                }
                $user_info = pdo_fetch("select * from " . tablename('ewei_shop_member') . " where id = " . $order['user_id']);
                if ($user_info) {
                    $res = pdo_update('red_order_detail', array('status' => $status, 'remark' => $remark), array('id' => $id));
                    if ($res) {
                        $detail = pdo_fetch("select * from " . tablename('red_detail') . " where account_id = " . $order['oaccount_id'] . " and goods_id = " . $order['goods_id']);
                        if ($status == 1) {
                            $account_price = $user_info['account_price'];
                            $price = $account_price + $order['price'];
//                          pdo_update('red_detail', array('amount' => $detail['amount'] - $order['amount']), array('detail_id' => $detail['detail_id']));
//                          pdo_update('ewei_shop_member',array('account_price'=>$price),array('id'=>$user_info['id']));
                            m('member')->setCredit2($user_info['openid'], 'account_price', $order['price'], '销售提成');
                            pdo_insert('red_check_log', array('add_time' => time(), 'order_id' => $order['id'], 'order_num' => $order['order_num'], 'price' => $order['price']));
                        }
                        if($status == 2){
                            pdo_update('red_detail', array('amount' => $detail['amount'] + $order['amount']), array('detail_id' => $detail['detail_id']));
                        }

                        show_json(1, '审核成功');
                    } else {
                        show_json(0, '审核失败');
                    }
                } else {
                    show_json(0, '用户出错');
                }
            }
            include $this->template('finance/log/ordercheck');
        } else {
            show_json(0, '单子错误');
        }
    }

    public function orderUpdate()
    {
        global $_GPC, $_W;
        $type = $_GPC['type'];
        $id = $_GPC['detail_id'];
        $value = $_GPC['value'];
        $detail_info = pdo_get('red_order_detail', array('id' => $id));
        if ($type == 1) {
            $data['single_price'] = $value;
            $old_data = $detail_info['single_price'];
        }
        if ($type == 2) {
            $data['amount'] = $value;
            $old_data = $detail_info['amount'];
        }
        if ($type == 3) {
            $data['price'] = $value;
            $old_data = $detail_info['price'];
        }
        if ($type == 4) {
            $data['goods_id'] = $value;
            $old_data = $detail_info['goods_id'];
        }
        $log = array(
            'order_detail_id' => $id,
            'type' => $type,
            'old_data' => $old_data,
            'new_data' => $value,
            'createtime' => time(),
            'updateby' => $_W['username'],

        );
        if ($type == 4) {
            $log['old_data'] = pdo_getcolumn('red_goods', array('goods_id' => $old_data), 'goods_name');
            $log['new_data'] = pdo_getcolumn('red_goods', array('goods_id' => $value), 'goods_name');
        }
        $res = pdo_update('red_order_detail', $data, array('id' => $id));
        if ($res) {
            pdo_insert('red_order_detail_log', $log);
            show_json(1);
        }

        show_json(0, '修改失败');

    }

//    public function status0()
//    {
//        $this->orderlist(0, 'status0');
//    }
//
//    public function status1()
//    {
//        $this->orderlist(1, 'status1');
//    }
//
//    public function status2()
//    {
//        $this->orderlist(2, 'status2');
//    }


}

?>
