<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}

class Withdraw_EweiShopV2Page extends WebPage
{
    public function main($type = 1)
    {

        global $_W;
        global $_GPC;
        $pindex = max(1, intval($_GPC['page']));
        $psize = 20;
        $condition = ' and log.uniacid=:uniacid and log.type=:type and log.money<>0';
        $condition1 = '';
        $params = array(':uniacid' => $_W['uniacid'], ':type' => $type);
        if (!empty($_GPC['keyword'])) {
            $_GPC['keyword'] = trim($_GPC['keyword']);
            if ($_GPC['searchfield'] == 'logno') {
                $condition .= ' and log.logno like :keyword';
            } else {
                if ($_GPC['searchfield'] == 'member') {
                    $condition1 .= ' and (realname like :keyword or nickname like :keyword or mobile like :keyword)';
                }
            }
            $params[':keyword'] = '%' . $_GPC['keyword'] . '%';
        }
        if (empty($starttime) || empty($endtime)) {
            $starttime = strtotime('-1 month');
            $endtime = time();
        }
        if (!empty($_GPC['time']['start']) && !empty($_GPC['time']['end'])) {
            $starttime = strtotime($_GPC['time']['start']);
            $endtime = strtotime($_GPC['time']['end']);
            $condition .= ' AND log.createtime >= :starttime AND log.createtime <= :endtime ';
            $params[':starttime'] = $starttime;
            $params[':endtime'] = $endtime;
        }
        if (!empty($_GPC['level'])) {
            $condition1 .= ' and level=' . intval($_GPC['level']);
        }
        if (!empty($_GPC['groupid'])) {
            $condition1 .= ' and groupid=' . intval($_GPC['groupid']);
        }
        $member_sql = '';
        if ($condition1 != '') {
            $member_sql = ' and openid IN (SELECT openid FROM ims_ewei_shop_member WHERE uniacid = :uniacid ' . $condition1 . ') OR openid IN (SELECT CONCAT(\'sns_wa_\',openid_wa) FROM ims_ewei_shop_member WHERE uniacid = :uniacid ' . $condition1 . ')';
        }
        if (!empty($_GPC['rechargetype'])) {
            $_GPC['rechargetype'] = trim($_GPC['rechargetype']);
            if ($_GPC['rechargetype'] == 'system1') {
                $condition .= ' AND log.rechargetype=\'system\' and log.money<0';
            } else {
                $condition .= ' AND log.rechargetype=:rechargetype';
                $params[':rechargetype'] = $_GPC['rechargetype'];
            }
        }
        if ($_GPC['status'] != '') {
            $condition .= ' and log.status=' . intval($_GPC['status']);
        }
        $sql = 'select log.id,m.id as mid, m.realname,m.avatar,m.weixin,log.logno,log.type,log.status,log.rechargetype,log.sendmoney,m.nickname,m.mobile,g.groupname,log.money,log.createtime,l.levelname,log.realmoney,log.deductionmoney,log.charge,log.remark,log.alipay,log.bankname,log.bankcard,log.realname as applyrealname,log.applytype from ' . tablename('ewei_shop_member_log') . ' log ' . ' left join ' . tablename('ewei_shop_member') . ' m on m.openid=log.openid' . ' left join ' . tablename('ewei_shop_member_group') . ' g on m.groupid=g.id' . ' left join ' . tablename('ewei_shop_member_level') . ' l on m.level =l.id' . ' where 1 ' . $condition . ' ORDER BY log.createtime DESC ';
        $sql = 'select log.id,log.openid,log.logno,log.type,log.status,log.rechargetype,log.sendmoney,log.money,log.createtime,log.realmoney,log.deductionmoney,log.charge,log.remark,log.alipay,log.bankname,log.bankcard,log.realname as applyrealname,log.applytype from ' . tablename('ewei_shop_member_log') . ' log ' . ' where 1 ' . $condition . ' ' . $member_sql . ' ORDER BY log.createtime DESC ';
        if (empty($_GPC['export'])) {
            $sql .= 'LIMIT ' . ($pindex - 1) * $psize . ',' . $psize;
        }
        $list = pdo_fetchall($sql, $params);
        $apply_type = array(0 => '微信钱包', 2 => '支付宝', 3 => '银行卡');
        if (!empty($list)) {
            $openids = array();
            foreach ($list as $key => $value) {
                $list[$key]['typestr'] = $apply_type[$value['applytype']];
                if ($value['deductionmoney'] == 0) {
                    $list[$key]['realmoney'] = $value['money'];
                }
                if (!strexists($value['openid'], 'sns_wa_')) {
                    array_push($openids, $value['openid']);
                } else {
                    array_push($openids, substr($value['openid'], 7));
                }
            }
            $members_sql = 'select id as mid, realname,avatar,weixin,nickname,mobile,openid,openid_wa from ' . tablename('ewei_shop_member') . ' where uniacid=:uniacid and openid IN (\'' . implode('\',\'', array_unique($openids)) . '\') OR openid_wa IN (\'' . implode('\',\'', array_unique($openids)) . '\')';
            $members = pdo_fetchall($members_sql, array(':uniacid' => $_W['uniacid']), 'openid');
            $rs = array();
            if (!empty($members)) {
                foreach ($members as $key => &$row) {
                    if (!empty($row['openid_wa'])) {
                        $rs['sns_wa_' . $row['openid_wa']] = $row;
                    } else {
                        $rs[] = $row;
                    }
                }
            }
            $member_openids = array_keys($members);
            foreach ($list as $key => $value) {
                if (in_array($list[$key]['openid'], $member_openids)) {
                    $list[$key] = array_merge($list[$key], $members[$list[$key]['openid']]);
                } else {
                    $list[$key] = array_merge($list[$key], $rs[$list[$key]['openid']]);
                }
            }
        }
        if ($_GPC['export'] == 1) {
            if ($_GPC['type'] == 1) {
                plog('finance.log.withdraw.export', '导出提现记录');
            } else {
                plog('finance.log.recharge.export', '导出充值记录');
            }
            foreach ($list as &$row) {
                $row['createtime'] = date('Y-m-d H:i', $row['createtime']);
                $row['groupname'] = empty($row['groupname']) ? '无分组' : $row['groupname'];
                $row['levelname'] = empty($row['levelname']) ? '普通会员' : $row['levelname'];
                $row['typestr'] = $apply_type[$row['applytype']];
                if ($row['status'] == 0) {
                    if ($row['type'] == 0) {
                        $row['status'] = '未充值';
                    } else {
                        $row['status'] = '申请中';
                    }
                } else {
                    if ($row['status'] == 1) {
                        if ($row['type'] == 0) {
                            $row['status'] = '充值成功';
                        } else {
                            $row['status'] = '完成';
                        }
                    } else {
                        if ($row['status'] == -1) {
                            if ($row['type'] == 0) {
                                $row['status'] = '';
                            } else {
                                $row['status'] = '失败';
                            }
                        }
                    }
                }
                if ($row['rechargetype'] == 'system') {
                    $row['rechargetype'] = '后台';
                } else {
                    if ($row['rechargetype'] == 'wechat') {
                        $row['rechargetype'] = '微信';
                    } else {
                        if ($row['rechargetype'] == 'alipay') {
                            $row['rechargetype'] = '支付宝';
                        }
                    }
                }
            }
            unset($row);
            $columns = array();
            $columns[] = array('title' => '昵称', 'field' => 'nickname', 'width' => 12);
            $columns[] = array('title' => '姓名', 'field' => 'realname', 'width' => 12);
            $columns[] = array('title' => '手机号', 'field' => 'mobile', 'width' => 12);
            $columns[] = array('title' => '会员等级', 'field' => 'levelname', 'width' => 12);
            $columns[] = array('title' => '会员分组', 'field' => 'groupname', 'width' => 12);
            $columns[] = array('title' => empty($type) ? '充值金额' : '提现金额', 'field' => 'money', 'width' => 12);
            if (!empty($type)) {
                $columns[] = array('title' => '到账金额', 'field' => 'realmoney', 'width' => 12);
                $columns[] = array('title' => '手续费金额', 'field' => 'deductionmoney', 'width' => 12);
                $columns[] = array('title' => '提现方式', 'field' => 'typestr', 'width' => 12);
                $columns[] = array('title' => '提现姓名', 'field' => 'applyrealname', 'width' => 24);
                $columns[] = array('title' => '支付宝', 'field' => 'alipay', 'width' => 24);
                $columns[] = array('title' => '银行', 'field' => 'bankname', 'width' => 24);
                $columns[] = array('title' => '银行卡号', 'field' => 'bankcard', 'width' => 24);
                $columns[] = array('title' => '申请时间', 'field' => 'applytime', 'width' => 24);
            }
            $columns[] = array('title' => empty($type) ? '充值时间' : '提现申请时间', 'field' => 'createtime', 'width' => 12);
            if (empty($type)) {
                $columns[] = array('title' => '充值方式', 'field' => 'rechargetype', 'width' => 12);
            }
            $columns[] = array('title' => '备注', 'field' => 'remark', 'width' => 24);
            m('excel')->export($list, array('title' => (empty($type) ? '会员充值数据-' : '会员提现记录') . date('Y-m-d-H-i', time()), 'columns' => $columns));
        }
        $total = pdo_fetchcolumn('select count(*) from ' . tablename('ewei_shop_member_log') . ' log ' . ' where 1 ' . $condition . ' ' . $member_sql, $params);
        $pager = pagination($total, $pindex, $psize);
        $groups = m('member')->getGroups();
        $levels = m('member')->getLevels();
        include $this->template();
    }

    public function wechat()
    {
        global $_GPC, $_W;
        $log = pdo_get('ewei_shop_member_log', array('id' => $_GPC['id']));
        $realmoney = $log['realmoney'];
        $openid = $log['openid'];
        $res = m('finance')->wechatTransfers($openid, $realmoney * 100);
        if ($res['code'] == '200') {
            $objectxml = (array)simplexml_load_string($res['content'], 'SimpleXMLElement', LIBXML_NOCDATA);
            if ($objectxml) {
                $result_code = strtolower($objectxml['result_code']);
                if ($result_code == 'success') {
                    pdo_update('ewei_shop_member_log',array('status'=>1),array('id'=>$_GPC['id']));
                    show_json(1,'提现成功');
                } else {
                    show_json(0,$objectxml['err_code_des']);
                }
            } else {
                show_json(0,'提现失败');
            }
        } else {
            show_json(0,'提现失败');
        }
    }

    public function alipay()
    {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['id']);
        $log = pdo_fetch('select * from ' . tablename('ewei_shop_member_log') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));
        $sec = m('common')->getSec();
        $sec = iunserializer($sec['sec']);
        if (empty($sec['alipay_pay']['open'])) {
            show_json(0, '未开启,支付宝打款!');
        }
        if (empty($log)) {
            show_json(0, '未找到记录!');
        }
        if ($log['deductionmoney'] == 0) {
            $realmoeny = $log['money'];
        } else {
            $realmoeny = $log['realmoney'];
        }

        $batch_no_money = $realmoeny * 100;
        $batch_no = 'D' . date('Ymd') . 'RW' . $log['id'] . 'MONEY' . $batch_no_money.rand(1,100);

        $res = m('finance')->singleTransfer($batch_no, $log['alipay'], $log['realname'], $log['realmoney'], $log['title']);
        if ($res->code == 10000) {
            pdo_update('ewei_shop_member_log', array('status' => 1), array('id' => $id));
            show_json(1, '提现成功');
        } else {
            show_json(0, $res->sub_msg);
        }
    }

    /**
     * 拒绝
     */
    public function refuse()
    {
        global $_W;
        global $_GPC;
        $id = intval($_GPC['id']);
        $log = pdo_fetch('select * from ' . tablename('ewei_shop_member_log') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));
        $member = pdo_fetch('SELECT * FROM ' . tablename('ewei_shop_member') . ' WHERE uniacid =:uniacid AND openid=:openid', array(':uniacid' => $_W['uniacid'], ':openid' => $log['openid']));
        if (empty($log)) {
            show_json(0, '未找到记录!');
        }

        if ($log['status'] == -1) {
            show_json(0, '退款申请已经处理!');
        }

        pdo_update('ewei_shop_member_log', array('status' => -1), array('id' => $id, 'uniacid' => $_W['uniacid']));

        if (0 < $log['money']) {
            $new_money = $member['account_price'] + $log['money'];
            pdo_update('ewei_shop_member',array('account_price'=>$new_money),array('openid'=>$log['openid']));
        }


        m('notice')->sendMemberLogMessage($log['id']);
        plog('finance.log.refuse', '拒绝余额度提现 ID: ' . $log['id'] . ' 金额: ' . $log['money'] . ' <br/>会员信息:  ID: ' . $member['id'] . ' / ' . $member['openid'] . '/' . $member['nickname'] . '/' . $member['realname'] . '/' . $member['mobile']);
        show_json(1);
    }
}

?>
