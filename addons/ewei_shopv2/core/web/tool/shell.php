<?php

if (!defined('IN_IA')) {
    exit('Access Denied');
}

class Test_EweiShopV2Page extends WebPage
{
    public function main()
    {
        $userInfo = pdo_fetchall("select * from " . tablename('eth_users') . " where status = 1");
        foreach ($userInfo as $user) {
            $sign_id = array();
            if ($user['type'] == 2) {
                $dicInfo = pdo_fetch("select * from " . tablename('eth_dict_detail') . " where id =" . $user['profession']);
                if ($dicInfo) {
                    $name = $dicInfo['name'];
                    $sign_id[] = $this->check_sign($name, 2);
                }

                if (!empty($user['job_professor'])) {
                    $strprof = $this->getJobProf($user['job_professor']);
                    $sign_id[] = $this->check_sign($strprof, 3);
                }

                $sign_id[] = $this->check_sign('HCP');

            } elseif ($user['type'] == 3) {
                $sign_id[] = $this->check_sign('员工');
            }
            $user['sign_str'] = $sign_str = implode(",", $sign_id);

            if(substr($sign_str,0,1) != ','&&$sign_str){
                $user['sign_str'] = ',' .$sign_str;
            }
            $array = array('user_id'=>$user['user_id'],'dicInfo'=>$dicInfo,'sign_str'=>$user);
            file_put_contents(IA_ROOT.'/log/testdata.log', json_encode($array).PHP_EOL,FILE_APPEND);

            $this->edit_user($user);
        }
    }

    /**
     * 粉丝编辑
     * @param $data
     * @return bool|int
     */
    function edit_user($data)
    {

        $userInfo = pdo_fetch('select openid from ' . tablename('eth_users') . " where user_id = " . $data['user_id']);
        if ($userInfo) {
            $openid = $userInfo['openid'];
            $account_api = WeAccount::create();
            $tags = array_filter(explode(',', $data['sign_str']));
            $fans_tags = array();
            foreach ($tags as $tag) {
                $res = pdo_fetch("select * from " . tablename('eth_tags') . " where id = " . $tag);
                $fans_tags[] = $res['tag_wxid'];
            }
            if (empty($data['sign_str'])) {
                foreach ($fans_tags as $tag) {
                    $result = $account_api->fansTagBatchUntagging(array($openid), $tag);
                }
            } else {
                $result = $account_api->fansTagTagging($openid, $fans_tags);
            }
//            if (!is_error($result)) {
                if ($data['type'] == 2) {
                    $res = pdo_update('eth_users', array('hospital' => $data['hospital'], 'profession' => $data['profession'], 'job_professor' => $data['job_profession'], 'email' => $data['email'], 'sign' => $data['sign_str'], 'user_name' => $data['user_name'], 'user_tel' => $data['user_tel']), array('user_id' => $data['user_id']));
                } else {
                    $res = pdo_update('eth_users', array('user_name' => $data['user_name'], 'user_tel' => $data['user_tel'], 'email' => $data['email'], 'sign' => $data['sign_str']), array('user_id' => $data['user_id']));
                }
//                return $res;
//            } else {
//                return 0;
//            }
        }


    }

    function check_sign($sign_name, $status = 0)
    {
        $tagInfo = pdo_fetch("select * from " . tablename('eth_tags') . " where tagname = '" . $sign_name . "'");
        if (empty($tagInfo)) {
            //创建标签
            $this->tag_add($sign_name, $status);
        }
        $new_tag = pdo_fetch("select * from " . tablename('eth_tags') . " where tagname = '" . $sign_name . "'");
        return $new_tag['id'];

    }

    function getJobProf($profIndex)
    {
        if ($profIndex == 5)
            return "主任医师";

        if ($profIndex == "4")
            return "副主任医师";

        if ($profIndex == "3")
            return "主治医师";

        if ($profIndex == "2")
            return "住院医师";

        if ($profIndex == "1")
            return "实习医师";

        return "";
    }


    function tag_add($tag_name, $status)
    {
        $account_api = WeAccount::create();
        $result = $account_api->fansTagAdd($tag_name);
        if (!is_error($result)) {
            $tag_id = $result['tag']['id'];
            return pdo_insert('eth_tags', array('tagname' => $tag_name, 'tag_wxid' => $tag_id, 'type' => $status));
        } else {
            return 0;
        }
    }
}

?>