<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}

class Index_EweiShopV2Page extends WebPage
{
    public function main()
    {
        global $_W,$_GPC;
        if ($_W['ispost']) {
            $is_cover = intval($_GPC['is_cover']);
            if(empty($is_cover)){
                $this->message('是否覆盖不能为空');
            }
            $excel = m('excel')->import('excelfile');
            $time = time();
            $problem_data = m('store')->import_deal($excel,$is_cover,$time);
            $pro_count = count($problem_data['problem_data']);
            $suc_count = $problem_data['corret_count'];
            $all_count = $pro_count+$suc_count;
            if($pro_count>0){
                $arr = array('message'=>'导入数据共' . $all_count.'条，成功导入了'.$suc_count.'条，问题数据有'.$pro_count.'条','buttontext'=>'导出错误数据','type'=>1,'url'=>webUrl('tool/export',array('mark'=>$time)));
            }else{
                $arr = array('message'=>'导入数据共' . $all_count.'条，成功导入了'.$suc_count.'条，问题数据有'.$pro_count.'条');
            }
            $this->message($arr,'exit');
        }
        $res = pdo_fetchall("select * from ".tablename('red_account_log'));
        $pro_status = 0;
        if($res){
            $pro_status = 1;
        }
        include $this->template('tool/work_export');
    }

    /**
     * 往来错误数据导出
     * @return int
     */
    public function export(){
        global $_GPC;
        $acoount_logs = pdo_fetchall("select * from ".tablename('red_account_log')." where mark = ".$_GPC['mark']);
        $columns = array(
            array('title' => '单位编码', 'field' => 'sign_num', 'width' => 18),
            array('title' => '单位名称', 'field' => 'sign_name', 'width' => 23),
            array('title' => '联系人', 'field' => 'contact_name', 'width' => 15),
            array('title' => '联系电话', 'field' => 'contact_tel', 'width' => 18),
            array('title' => '移动电话', 'field' => 'mobile_tel', 'width' => 18),
            array('title' => '联系地址', 'field' => 'address', 'width' => 23),
            array('title' => '停用标记', 'field' => 'is_stop', 'width' => 13),
            array('title' => '退换货期限（天）', 'field' => 'limit_time', 'width' => 18),
            array('title' => '地区', 'field' => 'area', 'width' => 18),
            array('title' => '备注', 'field' => 'remark', 'width' => 23),
        );
        $list = array();
        foreach ($acoount_logs as $logs){
            $list[] = array(
                'sign_num'=>$logs['sign_num'],
                'sign_name'=>$logs['sign_name'],
                'contact_name'=>$logs['contact_name'],
                'contact_tel'=>$logs['contact_tel'],
                'mobile_tel'=>$logs['mobile_tel'],
                'address'=>$logs['address'],
                'is_stop'=>$logs['is_stop'],
                'limit_time'=>$logs['limit_time'],
                'area'=>$logs['area'],
                'remark'=>$logs['remark']
            );
        }
        m('excel')->export($list, array('title' => '往来单位错误数据', 'columns' => $columns));
    }

    /**
 * 商品错误数据导出
 * @return int
 */
    public function goods_export(){
        global $_GPC;
        $acoount_logs = pdo_fetchall("select * from ".tablename('red_goods_log')." where mark = ".$_GPC['mark']);
        $columns = array(
            array('title' => '商品名称', 'field' => 'goods_name', 'width' => 18),
            array('title' => '条码', 'field' => 'bar_code', 'width' => 23),
            array('title' => '零售价', 'field' => 'price', 'width' => 15),
            array('title' => '规格', 'field' => 'normal_size', 'width' => 18),
            array('title' => '状态', 'field' => 'status', 'width' => 18),
        );
        $list = array();
        foreach ($acoount_logs as $logs){
            $list[] = array(
                'goods_name'=>$logs['goods_name'],
                'bar_code'=>$logs['bar_code'],
                'price'=>$logs['price'],
                'normal_size'=>$logs['normal_size'],
                'status'=>$logs['status'],
            );
        }
        m('excel')->export($list, array('title' => '商品错误数据', 'columns' => $columns));
    }
    /**
     * 销售明细错误数据导出
     * @return int
     */
    public function detail_export(){
        global $_GPC;
        $acoount_logs = pdo_fetchall("select * from ".tablename('red_goods_detail_log')." where mark = ".$_GPC['mark']);
        $columns = array(
            array('title' => '条码', 'field' => 'bar_code', 'width' => 23),
            array('title' => '单位编码', 'field' => 'sign_num', 'width' => 15),
            array('title' => '数量', 'field' => 'amount', 'width' => 18),
        );
        $list = array();
        foreach ($acoount_logs as $logs){
            $list[] = array(
                'bar_code'=>$logs['bar_code'],
                'sign_num'=>$logs['sign_num'],
                'amount'=>$logs['amount'],
            );
        }
        m('excel')->export($list, array('title' => '销售明细错误数据', 'columns' => $columns));
    }


}

?>
