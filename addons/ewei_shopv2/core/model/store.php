<?php
class Store_EweiShopV2Model
{
    /**
	 * 导入的门店数据处理
     * @param $excel
     * @return array
     */
	public function import_deal($excel,$is_cover,$time){
        $problem_data = array();
        $correct_count = 0;
        foreach ($excel as $value){
            $data = array();
            if(empty($value[0])&&empty($value[1])&&empty($value[2])&&empty($value[3])&&empty($value[4])&&empty($value[5])&&empty($value[6])&&empty($value[7])&&empty($value[8])&&empty($value[9])){
                continue;
            }
            $error['sign_num'] = $data['sign_num'] = trim($value[0]);
            $error['sign_name'] = $update['sign_name'] = $data['sign_name'] = trim($value[1]);
            $error['contact_name'] = $update['contact_name'] = $data['contact_name'] = trim($value[2]);
            $error['contact_tel'] = $update['contact_tel'] = $data['contact_tel'] = trim($value[3]);
            $error['mobile_tel'] = $update['mobile_tel'] = $data['mobile_tel'] = trim($value[4]);
            $error['address'] = $update['address'] = $data['address'] = trim($value[5]);
            $error['limit_time'] = $update['limit_time'] = $data['limit_time'] = trim($value[7]);
            $error['area'] = $update['area'] = $data['area'] = trim($value[8]);
            $error['remark'] = $update['remark'] = $data['remark'] = trim($value[9]);
            $error['is_stop'] = $value[6];

            if(empty($value[0])||empty($value[1])){
                $problem_data[] = $value;
                $error['mark'] = $time;
                pdo_insert('red_account_log',$error);
                continue;
			}
            if(trim($value[6]) == '否'){
                $update['is_stop'] = $data['is_stop'] = 0;
            }elseif(trim($value[6]) == '是'){
                $update['is_stop'] = $data['is_stop'] = 1;
            }else{
                $problem_data[] = $value;
                $error['mark'] = $time;
                pdo_insert('red_account_log',$error);
                continue;
            }
            if(!is_integer($value[7]+0)){
                $problem_data[] = $value;
                $error['mark'] = $time;
                pdo_insert('red_account_log',$error);
                continue;
            }
            //判断导入的是否有重复数据，若有 替换
            $signInfo = pdo_fetch("select account_id,sign_num from ".tablename('red_account')." where is_delete = 0 and sign_num = '".$value[0]."'");
            if($signInfo){
                if($is_cover == 1){
                    pdo_update("red_account",$update,array('account_id'=>$signInfo['account_id']));
                }else{
                        $problem_data[] = $value;
                        $error['mark'] = $time;
                        pdo_insert('red_account_log',$error);
                        continue;
                }
            } else{
                pdo_insert('red_account',$data);
            }
            $correct_count++;
        }
        return array('problem_data'=>$problem_data,'corret_count'=>$correct_count);
	}
    /**
     * 导入的商品数据处理
     * @param $excel
     * @return array
     */
    public function import_goods($excel,$is_cover,$time){
        $problem_data = array();
        $correct_count = 0;
        foreach ($excel as $value){
            $data = array();
            $status = 0;
            if($value[5] == '是'){
            	$status = 1;
			}
            $error['goods_name'] = $update['goods_name'] = $data['goods_name'] = $value[0];
            $error['bar_code'] = $data['bar_code'] = $value[1];
            $error['price'] = $update['price'] = $data['price'] = $value[2];
            $error['normal_size'] = $update['normal_size'] =  $data['normal_size'] = $value[3];
            $error['status'] = $update['status'] =  $data['status'] = $status;
            if(empty($value[0])&&empty($value[1])&&empty($value[2])){
                continue;
            }
            if(empty($value[0])||empty($value[1])){
                $error['mark'] = $data['mark'] = $time;
                $problem_data[] = $value;
                pdo_insert('red_goods_log',$data);
                continue;
			}

            $signInfo = pdo_fetchall("select goods_id from ".tablename('red_goods')." where is_delete = 0 and bar_code = '".$value[1]."'");
            if($signInfo){
                if($is_cover == 1){
                    pdo_update("red_goods",$update,array('goods_id'=>$signInfo['goods_id']));
                }else{
                    $error['mark'] = $data['mark'] = $time;
                    $problem_data[] = $value;
                    pdo_insert('red_goods_log',$data);
                    continue;
                }
            }else{
                pdo_insert('red_goods',$data);
            }
            $correct_count++;
        }
        return array('problem_data'=>$problem_data,'corret_count'=>$correct_count);
    }

    /**
     * 导入商品明细，计算库存
     * @param $excel
     * @return array
     */
    public function import_detail($excel,$time){
        global $_GPC;
        $problem_data = array();
        $correct_count = 0;
        $record_num = $this->get_record_sn();
        foreach ($excel as $value){
            if(empty($value[0])&&empty($value[1])&&empty($value[2])){
                continue;
            }
            if(empty($value[0])){
                $problem_data[] = $value;
                pdo_insert('red_goods_detail_log',array('bar_code'=>$value[0],'sign_num'=>$value[1],'amount'=>$value[2],'mark'=>$time,'type'=>1));
                continue;
            }
            if(empty($value[1])){
                $problem_data[] = $value;
                pdo_insert('red_goods_detail_log',array('bar_code'=>$value[0],'sign_num'=>$value[1],'amount'=>$value[2],'mark'=>$time,'type'=>2));
                continue;
            }

            if(is_int($value[2]+0)==false||$value[2]==0){
                $problem_data[] = $value;
                pdo_insert('red_goods_detail_log',array('bar_code'=>$value[0],'sign_num'=>$value[1],'amount'=>$value[2],'mark'=>$time,'type'=>3));
                continue;
            }
           $account_info =  pdo_fetch("select * from ".tablename('red_account')." where sign_num = '".$value[1]."'");
           $goods_info = pdo_fetch("select * from ".tablename('red_goods')." where bar_code = '".$value[0]."'");
           if(empty($account_info)){
               $problem_data[] = $value;
               pdo_insert('red_goods_detail_log',array('bar_code'=>$value[0],'sign_num'=>$value[1],'amount'=>$value[2],'mark'=>$time,'type'=>4));
               continue;
           }
           if(empty($goods_info)){
               $problem_data[] = $value;
               pdo_insert('red_goods_detail_log',array('bar_code'=>$value[0],'sign_num'=>$value[1],'amount'=>$value[2],'mark'=>$time,'type'=>5));
               continue;
           }
           //判断店铺的状态
            if($account_info['parent_id'] == 0){
               //一级店铺
                $account_id = $account_info['account_id'];
            }else{
               //已经被合并了
                if($account_info['is_bind'] == 1){
                    $account_id = $account_info['account_id'];
                }else{
                    $account_id = $account_info['parent_id'];
                }
            }
          $detail_info = pdo_fetch("select * from ".tablename('red_detail')." where account_id = ".$account_id." and goods_id = ".$goods_info['goods_id']);

          if($detail_info){
              //更新
              pdo_update("red_detail",array('amount'=>$detail_info['amount']+intval($value[2])),array('detail_id'=>$detail_info['detail_id']));
          }else{
              //插入
              pdo_insert('red_detail',array('goods_id'=>$goods_info['goods_id'],'account_id'=>$account_id,'amount'=>intval($value[2])));
          }

          pdo_insert('red_record',array('record_num'=>$record_num,'add_time'=>time(),'type'=>0,'uid'=>$_GPC['__uid'],'amount'=>intval($value[2]),'bar_code'=>$value[0],'sign_num'=>$value[1]));
          $correct_count++;
        }
        return array('problem_data'=>$problem_data,'corret_count'=>$correct_count);
    }

    //生成单号
    function get_record_sn()
    {
        return date('YmdHis') . $this->create_random(5, '0123456789');
    }

   //创建随机数
    function create_random($length, $char_str = 'abcdefghijklmnopqrstuvwxyz0123456789')
    {
        $hash = '';
        $chars = $char_str;
        $max = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $hash .= substr($chars, (rand(0, 1000) % $max), 1);
        }
        return $hash;
    }

	public function getStoreInfo($id)
	{
		global $_W;
		return pdo_fetch('select * from ' . tablename('ewei_shop_store') . ' where id=:id and uniacid=:uniacid Limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));
	}

	public function getGoodsInfo($id)
	{
		global $_W;
		$sql = 'select * from ' . tablename('ewei_shop_goods') . ' where id=:id and uniacid=:uniacid Limit 1';
		return pdo_fetch($sql, array(':id' => $id, ':uniacid' => $_W['uniacid']));
	}

	public function getStoreGoodsInfo($goodsid, $storeid, $flag = 0)
	{
		global $_W;

		if (empty($flag)) {
			$con = ' and gstatus=1';
		}
		else {
			$con = '';
		}

		$sql = 'select * from ' . tablename('ewei_shop_newstore_goods') . ' where goodsid=:goodsid and storeid=:storeid and uniacid=:uniacid ' . $con . ' Limit 1';
		return pdo_fetch($sql, array(':goodsid' => $goodsid, ':storeid' => $storeid, ':uniacid' => $_W['uniacid']));
	}

	public function getStoreGoodsOption($goodsid, $storeid)
	{
		global $_W;
		$sql = 'select * from ' . tablename('ewei_shop_newstore_goods_option') . ' where goodsid=:goodsid and storeid=:storeid and uniacid=:uniacid';
		return pdo_fetchall($sql, array(':goodsid' => $goodsid, ':storeid' => $storeid, ':uniacid' => $_W['uniacid']));
	}

	public function getOneStoreGoodsOption($optionid, $goodsid, $storeid)
	{
		global $_W;
		$sql = 'select * from ' . tablename('ewei_shop_newstore_goods_option') . ' where goodsid=:goodsid and storeid=:storeid and optionid=:optionid and uniacid=:uniacid Limit 1';
		return pdo_fetch($sql, array(':goodsid' => $goodsid, ':storeid' => $storeid, ':optionid' => $optionid, ':uniacid' => $_W['uniacid']));
	}

	public function getAllStore()
	{
		global $_W;
		$uniacid = $_W['uniacid'];
		$sql = 'select * from ' . tablename('ewei_shop_store') . ' where uniacid=:uniacid';
		return pdo_fetchall($sql, array(':uniacid' => $_W['uniacid']));
	}

	public function checkStoreid()
	{
		global $_W;
		global $_GPC;
		$newstoreid = intval($_SESSION['newstoreid']);

		if (empty($newstoreid)) {
			$newstoreid = intval($_GPC['storeid']);

			if (!empty($newstoreid)) {
				$_SESSION['newstoreid'] = $newstoreid;
			}
		}

		return $newstoreid;
	}

	public function getStoreName($list, $return = 'all')
	{
		global $_W;

		if (!is_array($list)) {
			return $this->getListUserOne($list);
		}

		$store = array();

		foreach ($list as $value) {
			$storeid = $value['storeid'];

			if (empty($storeid)) {
				$storeid = 0;
			}

			if (empty($store[$storeid])) {
				$store[$storeid] = array();
			}

			array_push($store[$storeid], $value);
		}

		if (!empty($store)) {
			$store_ids = array_keys($store);
			$store_list = pdo_fetchall('select * from ' . tablename('ewei_shop_store') . ' where uniacid=:uniacid and id in(' . implode(',', $store_ids) . ')', array(':uniacid' => $_W['uniacid']), 'id');
			$all = array('store' => $store, 'store_list' => $store_list);
			return $return == 'all' ? $all : $all[$return];
		}

		return array();
	}

	public function getListStoreOne($storeid)
	{
		global $_W;
		$storeid = intval($storeid);

		if ($storeid) {
			$store = pdo_fetch('select * from ' . tablename('ewei_shop_store') . (' where uniacid=:uniacid and id=' . $storeid), array(':uniacid' => $_W['uniacid']));
			return $store;
		}

		return false;
	}
}

if (!defined('IN_IA')) {
	exit('Access Denied');
}

?>
