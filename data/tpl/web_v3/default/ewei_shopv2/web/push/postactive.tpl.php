<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<script type="text/javascript" src="../addons/ewei_shopv2/static/js/dist/area/cascade.js"></script>
<style>
    .checkbox-inline{
        display: block;
    }
    .btns a i{
        display: inline-block;
        width: 100%;
        height: 20px;
        background: #f95959;
    }
    .btn-color {
        width: 25px;
        height: 25px;
        border: 1px solid #fff;
        margin: 2px;
        padding: 0;
    }

</style>
<div class="page-header">
    当前位置：<span class="text-primary">创建计划
    </span>
</div>
<div class="page-content">
<form action="" method="post" class="form-horizontal form-validate" enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-lg control-label">优先级</label>
        <div class="col-sm-9 col-xs-12">
            <select  name="sort" class="form-control tp_is_default" style="width:90px;">
                <?php  if(is_array($level_array)) { foreach($level_array as $value) { ?>
                 <option value="<?php  echo $value;?>" <?php  if($activity['sort']==$value) { ?>selected<?php  } ?>><?php  echo $value;?></option>
                <?php  } } ?>
            </select>
            <span class='help-block'>数字越大等级越高</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg control-label must">活动名称</label>
        <div class="col-sm-9 col-xs-12">
            <input type="text" name="active_name" class="form-control" value="<?php  echo $activity['active_name'];?>"  data-rule-required="true"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg control-label">活动图片</label>
        <div class="col-sm-9 col-xs-12">
            <?php  echo tpl_form_field_image2('thumb', $activity['thumb'])?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg control-label must">活动时间</label>
        <div class="col-sm-9 col-xs-12">
            <?= tpl_form_field_daterange('time', array('starttime'=>$item?date('Y-m-d',$activity['start_time']):date('Y-m-d',time()),'endtime'=>$activity?date('Y-m-d', $activity['end_time']):date('Y-m-d', time()+(3600*24*7))),true);?>
        </div>
    </div>


    <div class="form-group">
        <label class="col-lg control-label must">选择单位</label>
        <div class="col-sm-9 col-xs-12">
            <div>
                <?php  echo tpl_selector_new('account_id',array('preview'=>true,
                'readonly'=>true,
                'multi'=>1,
                'type'=>'account',
                'value'=>$next_account['sign_name'],
                'url'=>webUrl('store/queryactivity'),
                'optionurl'=>'sale.package.hasoption',
                'items'=>$ds,
                'text'=>'sign_name',
                'nokeywords'=>1,
                'autosearch'=>1,
                'buttontext'=>'选择单位',
                'placeholder'=>'请输入单位')
                )
                ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg control-label must">选择商品</label>
        <div class="col-sm-9 col-xs-12">
            <div>
                <?php  echo tpl_selector_new('goods_id',array('preview'=>true,  'readonly'=>true,'key'=>'bar_code','selectorid'=>'goods_id', 'multi'=>1,'autosearch'=>1, 'required'=>true,'type'=>'goods', 'text'=>'goods_name','placeholder'=>'商品名称/条码','buttontext'=>'选择商品 ', 'items'=>$item,'url'=>webUrl('push/querygoods')))?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg control-label must">备注</label>
        <div class="col-sm-9 col-xs-12">
            <textarea type="text" name="remark" class="form-control"  data-rule-required="true"><?php  echo $activity['remark'];?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg control-label"></label>
        <div class="col-sm-9 col-xs-12">
            <?php  if(empty($activity['activity_id'])) { ?>
            <input type="submit" value="提交" class="btn btn-primary"/>
            <?php  } ?>
            <a class="btn btn-default  btn-sm" href="<?php  echo webUrl('push/index')?>">返回列表</a>
        </div>
    </div>
</form>
</div>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>