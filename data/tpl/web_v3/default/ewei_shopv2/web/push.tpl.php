<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<style>
    tbody tr td{
        position: relative;
    }
    tbody tr  .icow-weibiaoti--{
        visibility: hidden;
        display: inline-block;
        color: #fff;
        height:18px;
        width:18px;
        background: #e0e0e0;
        text-align: center;
        line-height: 18px;
        vertical-align: middle;
    }
    tbody tr:hover .icow-weibiaoti--{
        visibility: visible;
    }
    tbody tr  .icow-weibiaoti--.hidden{
        visibility: hidden !important;
    }
    .full .icow-weibiaoti--{
        margin-left:10px;
    }
    .full>span{
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        vertical-align: middle;
        align-items: center;
    }
    tbody tr .label{
        margin: 5px 0;
    }
    .goods_attribute a{
        cursor: pointer;
    }
    .newgoodsflag{
        width: 22px;height: 16px;
        background-color: #ff0000;
        color: #fff;
        text-align: center;
        position: absolute;
        bottom: 70px;
        left: 57px;
        font-size: 12px;
    }
    .modal-dialog {
        min-width: 720px !important;
        position: absolute;
        left: 0;
        right: 0;
        top: 50%;
    }
    .catetag{
        overflow:hidden;

        text-overflow:ellipsis;

        display:-webkit-box;

        -webkit-box-orient:vertical;

        -webkit-line-clamp:2;
    }
</style>
<div class="page-header">
    当前位置：<span class="text-primary">计划管理</span>
</div>
<div class="page-content">
    <div class="fixed-header">
        <th style="width:50px;"></th>
        <th style="width: 50px;">排序</th>
        <th style="width:80px;">活动名称</th>
        <th style="">&nbsp;</th>
        <th style="width:130px;">开始时间</th>
        <th style="width: 130px;">结束时间</th>
        <th  style="width:130px;" >状态</th>
        <th style="width: 120px;">操作</th>

    </div>
    <form action="./index.php" method="get" class="form-horizontal form-search" role="form">
        <input type="hidden" name="c" value="site" />
        <input type="hidden" name="a" value="entry" />
        <input type="hidden" name="m" value="ewei_shopv2" />
        <input type="hidden" name="do" value="web" />
        <input type="hidden" name="r"  value="push.index" />
        <div class="page-toolbar">

            <div class="form-group">
                <button class="btn btn-primary pull-right"type="submit"> 搜索</button>


                <div class="input-group col-sm-4 pull-right">
                    <input type="text" class="input-sm form-control" name='keyword' value="<?php  echo $_GPC['keyword'];?>" placeholder="名称">
                    <span class="input-group-btn">
                </span>
                </div>
                <span class="input-group col-sm-3 pull-right">
                <?php  echo tpl_daterange('time', array('sm'=>true, 'placeholder'=>'开始时间 至 结束时间'),true);?>
                </span>

            </div>

        </div>
    </form>
    <?php  if(count($activity)>0) { ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-responsive">
                <thead class="navbar-inner">
                <tr>
                    <th style="width:50px;"></th>
                    <th style="width: 50px;">排序</th>
                    <th style="width:80px;">活动名称</th>
                    <th style="">&nbsp;</th>
                    <th style="width:130px;">门店数</th>
                    <th style="width:100px;">商品数</th>
                    <th style="width:100px;">开始时间</th>
                    <th style="width: 130px;">结束时间</th>
                    <th  style="width:130px;" >状态</th>
                    <th style="width: 120px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <?php  if(is_array($activity)) { foreach($activity as $item) { ?>
                <tr>
                    <td>
                        <input type='checkbox'  value="<?php  echo $item['activity_id'];?>"/>
                    </td>
                    <td>
                        <?php  echo $item['sort'];?>
                    </td>
                    <td>
                        <img src="<?php  echo tomedia($item['thumb'])?>" style="width:72px;height:72px;padding:1px;border:1px solid #efefef;margin: 7px 0" onerror="this.src='../addons/ewei_shopv2/static/images/nopic.png'" />
                    </td>
                    <td class='full' >
                        <span>
                            <span style="display: block;width: 100%;">
                                  <?php  echo $item['active_name'];?>
                            </span>
                        </span>
                    </td>
                    <td>
                        <?php  echo $item['account_num'];?>
                    </td>
                    <td>
                        <?php  echo $item['goods_num'];?>
                    </td>
                    <td>
                        <?php  echo date('Y-m-d', $item['start_time'])?>
                    </td>
                    <td>
                        <?php  echo date('Y-m-d', $item['end_time'])?>
                    </td>
                    <td>
                        <?php  echo $item['status'];?>
                    </td>
                    <td  style="overflow:visible;position:relative">
                        <a  class='btn btn-op btn-operation' href="<?php  echo webUrl('push/active_detail', array('activity_id' => $item['activity_id'],'goodsfrom'=>$goodsfrom,'page'=>$page))?>" >
                                         <span data-toggle="tooltip" data-placement="top" title="" data-original-title="查看">
                                            <i class="icow icow-bianji2"></i>
                                         </span>
                        </a>
                        <a  class='btn  btn-op btn-operation' data-toggle='ajaxRemove' href="<?php  echo webUrl('push/delactivity', array('activity_id' => $item['activity_id']))?>" data-confirm='确认删除该活动么？'>
                                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="删除">
                                     <i class='icow icow-shanchu1'></i>
                                </span>
                        </a>
                    </td>
                </tr>
                <?php  if(!empty($item['merchname']) && $item['merchid'] > 0) { ?>
                <tr style="background: #f9f9f9">
                    <td colspan='<?php  if($goodsfrom=='cycle') { ?>9<?php  } else { ?>10<?php  } ?>' style='text-align: left;border-top:none;padding:5px 0;' class='aops'>
                    <span class="text-default" style="margin-left: 10px;">商户名称：</span><span class="text-info"><?php  echo $item['merchname'];?></span>
                    </td>
                </tr>
                <?php  } ?>
                <?php  } } ?>
                <tfoot>
                <tr>
                    <td><input type="checkbox"></td>
                    <td    <?php  if($goodsfrom!='cycle') { ?>colspan="4"<?php  } else { ?>colspan="3" <?php  } ?>>
                    <div class="btn-group">
                        <?php if(cv('goods.edit')) { ?>
                        <?php  if($_GPC['goodsfrom']=='sale') { ?>
                        <button class="btn btn-default btn-sm  btn-operation" type="button" data-toggle='batch'  data-href="<?php  echo webUrl('goods/status',array('status'=>0))?>">
                            <i class='icow icow-xiajia3'></i> 下架</button>
                        <?php  } ?>
                        <?php  if($_GPC['goodsfrom']=='stock') { ?>
                        <button class="btn btn-default btn-sm  btn-operation" type="button" data-toggle='batch' data-href="<?php  echo webUrl('goods/status',array('status'=>1))?>">
                            <i class='icow icow-shangjia2'></i> 上架</button>
                        <?php  } ?>
                        <?php  } ?>

                        <?php  if($_GPC['goodsfrom']=='cycle') { ?>
                        <?php if(cv('goods.delete1')) { ?>
                        <button class="btn btn-default btn-sm  btn-operation" type="button" data-toggle='batch-remove' data-confirm="如果商品存在购买记录，会无法关联到商品, 确认要彻底删除吗?" data-href="<?php  echo webUrl('goods/delete1')?>">
                            <i class='icow icow-shanchu1'></i> 彻底删除</button>
                        <?php  } ?>

                        <?php if(cv('goods.restore')) { ?>
                        <button class="btn btn-default btn-sm  btn-operation" type="button" data-toggle='batch-remove' data-confirm="确认要恢复?" data-href="<?php  echo webUrl('goods/restore')?>">
                            <i class='icow icow-huifu1'></i> 恢复到仓库</button>
                        <?php  } ?>

                        <?php  } else { ?>
                        <?php if(cv('goods.delete')) { ?>
                        <button class="btn btn-default btn-sm  btn-operation" type="button" data-toggle='batch-remove' data-confirm="确认彻底删除此商品与门店的绑定关系?" data-href="<?php  echo webUrl('goods/deletebindgoods')?>">
                            <i class='icow icow-shanchu1'></i> 彻底删除</button>
                        <?php  } ?>
                        <?php  } ?>
                    </div>
                    </td>
                    <td colspan="4" style="text-align: right">
                        <?php  echo $pager;?>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <?php  } else { ?>
    <div class="panel panel-default">
        <div class="panel-body empty-data">暂时没有任何商品!</div>
    </div>
    <?php  } ?>
</div>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('goods/batchcates', TEMPLATE_INCLUDEPATH)) : (include template('goods/batchcates', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>
<script>
    $(function () {
        $.get("<?php  echo webUrl('goods/queryaccount')?>", {
            keyword: $.trim($('#search-kwd-notice').val()),
        }, function (dat) {
            $('#module-menus-notice').html(dat);
        });
    });

    //单位数据搜索
    function search() {
        if ($.trim($('#search-kwd-notice').val()) == '') {
            Tip.focus('#search-kwd-notice', '请输入关键词');
            return;
        }
        $("#module-menus-notice").html("正在搜索....");
        $.get("<?php  echo webUrl('goods/queryaccount')?>", {
            keyword: $.trim($('#search-kwd-notice').val()),
        }, function (dat) {
            $('#module-menus-notice').html(dat);
        });

    }

    //选择单位点击确定按钮
    function choose_account() {
        var account_id = $('.modal_account:checked').val()?$('.modal_account:checked').val():$('.account_id').val();
        $('.sign_name').val($('.modal_account:checked').attr('data-value'));
        //获取商品数据
        location.href =  '<?php  echo webUrl('goods/index')?>&account_id=' + account_id;
    }

    //获得分类标签
    // var length = $('#catetag').children().length;
    // if (length >10){
    //     for (var i=2;i<length;i++)
    //     {
    //         $('#catetag').children().eq(i).hide();
    //     }
    //     $('#catetag').append('...等');
    // }
    //显示批量分类
    $('#batchcatesbut').click(function () {
        $('#batchcates').show();
    })

    //关闭批量分类
    $('.modal-header .close').click(function () {
        $('#batchcates').hide();
    })

    // 取消批量分类
    $('.modal-footer .btn.btn-default').click(function () {
        $('#batchcates').hide();
    })


    //确认
    $('.modal-footer .btn.btn-primary').click(function () {
        var selected_checkboxs = $('.table-responsive tbody tr td:first-child [type="checkbox"]:checked');
        var goodsids = selected_checkboxs.map(function () {
            return $(this).val()
        }).get();

        var cates=$('#cates').val();
        var iscover=$('input[name="iscover"]:checked').val();
        $.post(biz.url('goods/ajax_batchcates'),{'goodsids':goodsids,'cates': cates,'iscover':iscover}, function (ret) {
            if (ret.status == 1) {
                $('#batchcates').hide();
                tip.msgbox.suc('修改成功');
                window.location.reload();
                return
            } else {
                tip.msgbox.err('修改失败');
            }
        }, 'json');
    })

    $(document).on("click", '[data-toggle="ajaxEdit2"]',
        function (e) {
            var _this = $(this)
            $(this).addClass('hidden')
            var obj = $(this).parent().find('a'),
                url = obj.data('href') || obj.attr('href'),
                data = obj.data('set') || {},
                html = $.trim(obj.text()),
                required = obj.data('required') || true,
                edit = obj.data('edit') || 'input';
            var oldval = $.trim($(this).text());
            e.preventDefault();

            submit = function () {
                e.preventDefault();
                var val = $.trim(input.val());
                if (required) {
                    if (val == '') {
                        tip.msgbox.err(tip.lang.empty);
                        return;
                    }
                }
                if (val == html) {
                    input.remove(), obj.html(val).show();
                    //obj.closest('tr').find('.icow').css({visibility:'visible'})
                    return;
                }
                if (url) {
                    $.post(url, {
                        value: val
                    }, function (ret) {

                        ret = eval("(" + ret + ")");
                        if (ret.status == 1) {
                            obj.html(val).show();

                        } else {
                            tip.msgbox.err(ret.result.message, ret.result.url);
                        }
                        input.remove();
                    }).fail(function () {
                        input.remove(), tip.msgbox.err(tip.lang.exception);
                    });
                } else {
                    input.remove();
                    obj.html(val).show();
                }
                obj.trigger('valueChange', [val, oldval]);
            },
                obj.hide().html('<i class="fa fa-spinner fa-spin"></i>');
            var input = $('<input type="text" class="form-control input-sm" style="width: 80%;display: inline;" />');
            if (edit == 'textarea') {
                input = $('<textarea type="text" class="form-control" style="resize:none;" rows=3 width="100%" ></textarea>');
            }
            obj.after(input);

            input.val(html).select().blur(function () {
                submit(input);
                _this.removeClass('hidden')

            }).keypress(function (e) {
                if (e.which == 13) {
                    submit(input);
                    _this.removeClass('hidden')
                }
            });

        })
</script>
