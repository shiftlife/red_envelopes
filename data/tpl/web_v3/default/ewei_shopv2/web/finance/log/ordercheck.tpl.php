<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<script type="text/javascript" src="../addons/ewei_shopv2/static/js/dist/area/cascade.js"></script>
<style type='text/css'>
    .ordertable { width:100%;position: relative;margin-bottom:10px}
    .ordertable tr td:first-child { text-align: right }
    .ordertable tr td {padding:10px 5px 0;vertical-align: top}
    .ordertable1 tr td { text-align: right; }
    .ops .btn { padding:5px 10px;}
    .order-container{
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }
    .order-container-left{
        -webkit-box-flex: 1;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }
    .order-container-static{
        -webkit-box-flex: 1;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        padding: 30px 50px 20px;
    }
    .font18{
        font-size:20px;
        font-weight:bold;;
    }
    .trbagpack span{
        margin: 0 10px;
        display: inline-block;
        vertical-align: middle;
    }
    .trbagpack span.address{
        width:150px;
        overflow: hidden;
        text-overflow:ellipsis;
        white-space: nowrap;
    }
    tfoot .price{
        float: right;
    }
    tfoot .price-inner{
        display: inline-block;
        vertical-align: middle;
        width:100px;
        text-align: right;
    }
    .packbag-group{
        border:1px solid #efefef;
    }
    .packbag{
        padding: 0 30px;
    }
    .packbag-title{
        line-height: 33px;
    }
    .packbag-group .packbag-list{
        padding: 20px 33px;
        border-bottom: 1px solid #efefef;
        display: flex;
        align-items: center;
    }
    .packbag-list .packbag-media{
        width:100px;
    }
    .packbag-list .packbag-inner{
        border-left:1px solid #efefef;
        -webkit-box-flex: 1;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }
    .packbag-goods-list{
        display: flex;
        flex-wrap: wrap;
        width:100%;
    }
    .packbag-goods{
        width:25%;
        display: flex;
        display: -webkit-flex;
        margin: 10px 0 5px;
    }
    .packbag-goods-media{
        width:52px;
        height: 52px;
        margin-right: 10px;
    }
    .packbag-goods-media img{
        width:52px;
        height: 52px;
        border: 1px solid #efefef;
    }
    .packbag-goods-inner{
        flex:1;
        -webkit-box-flex: 1;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        overflow: hidden;
    }
    .packbag-goods-inner p{
        color: #999;
    }
    .packbag-goods-inner .title{
        width:100%;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    .table .trorder td{
        border-right:1px solid #efefef;
    }
    .table .trorder td:nth-of-type(1){
        border:none;
    }
    tbody tr td{
        position: relative;
    }
    tbody tr  .icow-weibiaoti--{
        visibility: hidden;
        display: inline-block;
        color: #fff;
        height:18px;
        width:18px;
        background: #e0e0e0;
        text-align: center;
        line-height: 18px;
        vertical-align: middle;
    }
    tbody tr:hover .icow-weibiaoti--{
        visibility: visible;
    }
    tbody tr  .icow-weibiaoti--.hidden{
        visibility: hidden !important;
    }
    #done-err:before{
        background: red;
    }

</style>
<div class="page-header">
    当前位置：<span class="text-primary">提单处理
    </span>
</div>
<div class="page-content">
    <?php  if($item['status']!=-1) { ?>
    <div class="step-region" >
        <ul class="ui-step ui-step-4" >
            <li <?php  if($order['status']>=0) { ?>class="ui-step-done"<?php  } ?>>
            <div class="ui-step-number" >1</div>
            <div class="ui-step-title" >首次提单</div>
            </li>
            <li <?php  if($order['status']>=0) { ?>class="ui-step-done"<?php  } ?>>
            <div class="ui-step-number">2</div>
            <div class="ui-step-title">初审</div>
            </li>
            <?php  if($order['status'] == 0) { ?>
            <li>
                <div class="ui-step-number">3</div>
                <div class="ui-step-title">待审核</div>
            </li>
            <?php  } ?>
            <?php  if($order['status'] == 1) { ?>
               <li class="ui-step-done">
               <div class="ui-step-number">3</div>
               <div class="ui-step-title">审核通过</div>
               </li>
            <?php  } ?>
            <?php  if($order['status'] == 2) { ?>

            <li class="ui-step-done"  id="done-err" style="">
            <div class="ui-step-number" style=" background: red">3</div>
            <div class="ui-step-title">审核拒绝</div>
            </li>
            <?php  } ?>
        </ul>
    </div>
    <?php  } ?>
    <form class="form-horizontal form-validate" action="" method="post">
        <input type="hidden" name="id" value="<?php  echo $item['id'];?>" />
        <input type="hidden" name="dispatchid" value="<?php  echo $dispatch['id'];?>" />
        <input type="hidden" name="goods_id" value="<?php  echo $order['goods_id'];?>" />

        <h3 class="order-title">订单信息</h3>
        <div class="row order-container">
            <div class="order-container-left" style="border-right: 1px solid #efefef">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="">
                            <li class="text"><span class="col-sm" style="width: 100px">提单号：</span><span class="text-default"><?php  echo $order['order_num'];?></span></li>
                            <li class="text"><span class="col-sm" style="width: 100px">提单时间：</span><span class="text-default"><?php  echo date('Y-m-d h:i',$order['add_time'])?></span></li>
                            <li class="text"><span class="col-sm" style="width: 100px">提单人：</span><span class="text-default"><?php  echo $order['realname'];?></span></li>
                            <li class="text"><span class="col-sm" style="width: 100px">提单人门店：</span><span class="text-default"><?php  echo $order['sign_name'];?></span></li>
                            <li class="text"><span class="col-sm" style="width: 100px">提单人电话：</span><span class="text-default"><?php  echo $order['mobile'];?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <h3 class="order-title">商品信息</h3>
        <table class="table ">
            <thead>
            <tr class="trorder" style="background: #fff">
                <th class="" style="width: 75px;text-align: right;padding-right: 0">
                    商品标题
                </th>
                <th style=""></th>
                <th style="text-align: center;width: 10%">单个佣金</th>
                <th style="text-align: center;width: 10%">数量</th>
                <th style="text-align: center;width: 10%;">计算总额</th>
                <th style="text-align: center;width: 10%;">实际总额</th>
            </tr>
            </thead>
            <tbody>
            <tr class="trorder" style="text-align: right;padding-right: 0;height: 100px">
                <td style="text-align: right;padding-right: 0">
                    <img src="<?php  echo tomedia($order['thumb'])?>" style="width:52px;height:52px;border:1px solid #efefef; padding:1px;" onerror="this.src='../addons/ewei_shopv2/static/images/nopic.png'">
                </td>
                <td style="text-align: left" id="goods_name"><span><?php  echo $order['goods_name'];?></span> <?php  if($order['status']==0) { ?>
                    <!--<i class="icow icow-weibiaoti&#45;&#45;" id="modal"></i>-->
                    <?php  } ?>
                </td>
                <td style="text-align: center">
                    <?php  if($order['status']==1 | $order['status']==2) { ?>
                    <?php  echo sprintf("%.2f",$order['single_price'])?>
                    <?php  } else { ?>
                    <!--<a href='javascript:;' data-toggle='ajaxEdit' data-href="<?php  echo webUrl('finance/log/orderUpdate',array('detail_id'=>$order['id'],'type'=>1))?>"  >-->
                        <?php  echo sprintf("%.2f",$order['single_price'])?>
                    <!--</a>-->
                    <!--<i class="icow icow-weibiaoti&#45;&#45; "  data-toggle='ajaxEdit2'></i>-->
                    <?php  } ?>
                </td>
                <td style="text-align: center">
                    <?php  if($order['status']==1 | $order['status']==2) { ?>
                    <?php  echo $order['amount'];?>
                    <?php  } else { ?>
                    <!--<a href='javascript:;' data-toggle='ajaxEdit' data-href="<?php  echo webUrl('finance/log/orderUpdate',array('detail_id'=>$order['id'],'type'=>2))?>" >-->
                        <?php  echo $order['amount'];?>
                    <!--</a>-->
                    <!--<i class="icow icow-weibiaoti&#45;&#45; "  data-toggle='ajaxEdit2'></i>-->
                    <?php  } ?>
                </td>
                <td style="text-align: center"><?php  echo  sprintf("%.2f",$order['single_price']*$order['amount'])?></td>
                <td style="text-align: center">
                    <?php  if($order['status']==1 | $order['status']==2) { ?>
                    <?php  echo sprintf("%.2f",$order['price'])?>
                    <?php  } else { ?>
                    <a href='javascript:;' data-toggle='ajaxEdit' data-href="<?php  echo webUrl('finance/log/orderUpdate',array('detail_id'=>$order['id'],'type'=>3))?>" >
                        <?php  echo sprintf("%.2f",$order['price'])?>
                    </a>
                    <i class="icow icow-weibiaoti-- "  data-toggle='ajaxEdit2'></i>
                    <?php  } ?>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr class="trorder">
                <td colspan="6" style="padding-left: 20px">友情提示：点击商品行可修改商品单价，数量，佣金。</td>

            </tr>
            </tfoot>
        </table>

        <h3 class="order-title">操作日志</h3>
        <div class="row order-container" style="max-height: 300px;overflow-y:scroll">
            <div class="order-container-left" style="border-right: 1px solid #efefef">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>操作时间</th>
                                <th>操作人员</th>
                                <th>操作类型</th>
                                <th style="width: 600px">操作日志</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php  if(is_array($order_log)) { foreach($order_log as $ol) { ?>
                            <tr>
                                <td><?php  echo date('Y-m-d H:i',$ol['createtime'])?></td>
                                <td><?php  echo $ol['updateby'];?></td>
                                <td><?php  if($ol['type']==1) { ?>修改单个佣金<?php  } ?><?php  if($ol['type']==2) { ?>修改数量<?php  } ?><?php  if($ol['type']==3) { ?>修改总额<?php  } ?><?php  if($ol['type']==4) { ?>修改商品<?php  } ?></td>
                                <td><?php  if($ol['type']==1) { ?>修改单个佣金<?php  } ?><?php  if($ol['type']==2) { ?>修改数量<?php  } ?><?php  if($ol['type']==3) { ?>修改总额<?php  } ?><?php  if($ol['type']==4) { ?>修改商品<?php  } ?>：<?php  echo $ol['old_data'];?>-><?php  echo $ol['new_data'];?></td>
                            </tr>
                            <?php  } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <h3 class="order-title">审核</h3>
        <div class="form-group">
            <label class="col-lg">小票图片</label>
            <div class="col-sm-9 col-xs-12">
                <ul id="pic">
                <?php  if(is_array($order_pic)) { foreach($order_pic as $pic) { ?>
                    <?php  if($pic) { ?>
                    <li style="float: left;margin-right: 20px;border:2px solid #e0dfdf;margin-bottom: 20px;border-radius: 5px;overflow: hidden">
                        <img src="<?php  echo tomedia($pic)?>" alt="" style="width: 100px;height: 100px;">
                    </li>
                    <?php  } ?>
                <?php  } } ?>
                </ul>
            </div>
        </div>

        <div class="form-group store">
            <label class="col-lg control-label">审核状态</label>
            <div class="col-sm-9">
                <span class="input-group">
                    <label class="radio-inline"><input type="radio" name="status" class="btn-maxcredit" value="1" <?php  if($order['status'] == 1 ) { ?>checked<?php  } ?> >通过</label>
                    <label class="radio-inline"><input type="radio"  name="status"  class="btn-maxcredit" value="2" <?php  if($order['status'] == 2) { ?>checked<?php  } ?> >拒绝</label>
                </span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg control-label">备注</label>
            <div class="col-sm-9 col-xs-12">
                <textarea type="text" name="remark" class="form-control" ><?php  echo $order['remark'];?></textarea>
                <input type="hidden" name="id" class="form-control" value="<?php  echo $order['id'];?>"  data-rule-required="true"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg control-label"></label>
            <div class="col-sm-9 col-xs-12">

                <input type="submit" value="提交" class="btn btn-primary" id="submit-btn"  <?php  if($order['status']==1 | $order['status']==2) { ?>disabled<?php  } ?> />
                <a class="btn btn-default  btn-sm" href="javascript:history.back(-1)">返回上步</a>
                <a class="btn   btn-sm" href="javascript:void(0)" style="background: red;margin-left: 50px;color:#ffffff" id="xiaopiao">查看最近小票</a>
            </div>
        </div>
    </form>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">商品标题</h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th style="width: 300px">商品名称</th>
                        <th>条形码</th>
                        <th>价格</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php  if(is_array($goods)) { foreach($goods as $gs) { ?>
                    <tr>
                        <td name="goods_name"><?php  echo $gs['goods_name'];?></td>
                        <td><?php  echo $gs['bar_code'];?></td>
                        <td><?php  echo $gs['price'];?></td>
                        <td><a class="btn btn-primary btn-xs"  name="chose" href="javascript:void(0)" data-id="<?php  echo $gs['goods_id'];?>" <?php  if($order['goods_id']==$gs['goods_id']) { ?>disabled<?php  } ?>>选择</a></td>
                    </tr>
                    <?php  } } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 2000">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel2">最近小票</h4>
            </div>
            <div class="modal-body">
                <ul id="pic2">
                    <?php  if(is_array($xiaopiao_arr)) { foreach($xiaopiao_arr as $xp) { ?>
                    <?php  if($xp) { ?>
                    <li style="float: left;margin-right: 20px;border:2px solid #e0dfdf;margin-bottom: 20px;border-radius: 5px;overflow: hidden">
                        <img src="<?php  echo tomedia($xp)?>" alt="" style="width: 100px;height: 100px;">
                    </li>
                    <?php  } ?>
                    <?php  } } ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#modal').click(function () {
        $('#myModal').modal('show');
    })
    $('#xiaopiao').click(function () {
        $('#myModal2').modal('show');
    })
    $('[name="chose"]').click(function () {
        var that = $(this);
        var goods_id = that.attr('data-id');
        $.ajax({
            type: "post",
            url: "<?php  echo webUrl('finance/log/orderUpdate')?>",
            data: {
                'detail_id': '<?php  echo $order["id"];?>',
                'value': goods_id,
                'type': 4,
            },
            dataType: "json",
            success: function (data) {
                if(data.status == 1){
                    new_str = that.parent().siblings('[name="goods_name"]').text();
                    $('#goods_name span').text(new_str);
                    $('#myModal').modal('hide');
                    $('[name="chose"]').removeAttr('disabled');
                    that.attr('disabled','disabled');
                } else{
                    tip.msgbox.err('修改失败,可能选择了同样的商品');
                }
            }
        });
    })
</script>

<script>
    $('#pic').viewer();
    $('#pic2').viewer();
</script>

<script>
    $('#submit-btn').on('click',function () {
        if($('[name="status"]:checked').val()==2){
            if($('[name="remark"]').isEmpty()){
                tip.msgbox.err('请填写拒绝理由');
                return false;
            }
        }})

    $(document).on("click", '[data-toggle="ajaxEdit2"]',
        function (e) {
            var _this = $(this)
            $(this).addClass('hidden')
            var obj = $(this).parent().find('a'),
                url = obj.data('href') || obj.attr('href'),
                data = obj.data('set') || {},
                html = $.trim(obj.text()),
                required = obj.data('required') || true,
                edit = obj.data('edit') || 'input';
            var oldval = $.trim($(this).text());
            e.preventDefault();

            submit = function () {
                e.preventDefault();
                var val = $.trim(input.val());
                if (required) {
                    if (val == '') {
                        tip.msgbox.err(tip.lang.empty);
                        return;
                    }
                }
                if (val == html) {
                    input.remove(), obj.html(val).show();
                    //obj.closest('tr').find('.icow').css({visibility:'visible'})
                    return;
                }
                if (url) {
                    $.post(url, {
                        value: val
                    }, function (ret) {

                        ret = eval("(" + ret + ")");
                        if (ret.status == 1) {
                            obj.html(val).show();

                        } else {
                            tip.msgbox.err(ret.result.message, ret.result.url);
                        }
                        input.remove();
                    }).fail(function () {
                        input.remove(), tip.msgbox.err(tip.lang.exception);
                    });
                } else {
                    input.remove();
                    obj.html(val).show();
                }
                obj.trigger('valueChange', [val, oldval]);
            },
                obj.hide().html('<i class="fa fa-spinner fa-spin"></i>');
            var input = $('<input type="text" class="form-control input-sm" style="width: 80%;display: inline;" />');
            if (edit == 'textarea') {
                input = $('<textarea type="text" class="form-control" style="resize:none;" rows=3 width="100%" ></textarea>');
            }
            obj.after(input);

            input.val(html).select().blur(function () {
                submit(input);
                _this.removeClass('hidden')

            }).keypress(function (e) {
                if (e.which == 13) {
                    submit(input);
                    _this.removeClass('hidden')
                }
            });

        })
</script>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>