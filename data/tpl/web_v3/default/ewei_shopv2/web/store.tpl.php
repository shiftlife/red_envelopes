<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
 
<div class="page-header">
    当前位置：<span class="text-primary">门店管理</span>
</div>
<div class="page-content">
  <form action="" method="get">
   <input type="hidden" name="c" value="site" />
   <input type="hidden" name="a" value="entry" />
   <input type="hidden" name="m" value="ewei_shopv2" />
   <input type="hidden" name="do" value="web" />
   <input type="hidden" name="r" value="store" />
   <div class="page-toolbar">
        <div class="col-sm-6">
            <?php if(cv('store.add')) { ?>
            <a class='btn btn-primary btn-sm' href="<?php  echo webUrl('store/add')?>"><i class='fa fa-plus'></i> 添加门店</a>
            <?php  } ?>
            <a class='btn btn-primary btn-sm' href="<?php  echo webUrl('store/combind')?>"><i class='fa fa-plus'></i> 合并门店</a>
            <a class='btn btn-primary btn-sm' href="<?php  echo webUrl('store/store')?>"><i class='fa fa-plus'></i> 导入门店</a>

        </div>
        <div class="col-sm-6 pull-right">
            <div class="input-group">
                 <input type="text" class="input-sm form-control" name='keyword' value="<?php  echo $_GPC['keyword'];?>" placeholder="单位名称/单位编号/联系电话"> <span class="input-group-btn">
                 <button class="btn btn-primary" type="submit"> 搜索</button> </span>
            </div>
        </div>
        </div>
  </form>
 
         <?php  if(count($list)>0) { ?>
            <div class="page-table-header">
                <input type="checkbox">
                <div class="btn-group">
                    <?php if(cv('store.delete')) { ?>
                    <button class="btn btn-default btn-sm btn-operation" type="button" data-toggle='batch-remove' data-confirm="确认要删除?" data-href="<?php  echo webUrl('store/delete')?>">
                        <i class='icow icow-shanchu1'></i> 删除
                    </button>
                    <?php  } ?>
                </div>
            </div><!--Mi-->
            <table class="table table-responsive">
                <thead>
                    <tr>
				        <th style="width:25px;"></th>
                        <th style='width:80px;'>单位编号</th>
                        <th style='width:160px;'>单位名称</th>
                        <th style="width:150px;">联系人/电话/联系地址</th>
                        <th style="width:60px;">状态</th>
                        <th style="width:60px;">店员数</th>
                        <th style="width:60px;">商品数</th>
                        <th style="width:60px;">进行中活动</th>
                        <th style="width: 85px;">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  if(is_array($list)) { foreach($list as $row) { ?>
                    <tr>
                        <td>
                            <input type='checkbox'  name="account_id" value="<?php  echo $row['account_id'];?>"/>
                       </td>
                        <td><?php  echo $row['sign_num'];?></td>
                        <td><?php  echo $row['sign_name'];?></td>
                        <td><?php  echo $row['contact_name'];?><br><?php  echo $row['contact_tel'];?><br><?php  echo $row['address'];?></td>
                        <td>
                            <span class='label <?php   if($row['is_stop']==1) { ?>label-default<?php   } else { ?>label-primary<?php   } ?>'

                            data-toggle='ajaxSwitch'
                            data-confirm = "确认是<?php   if($row['is_stop']==1) { ?>启用<?php   } else { ?>停用<?php   } ?>？"
                            data-switch-refresh='true'
                            data-switch-value='<?php   echo $row['is_stop'];?>'
                            data-switch-value0='0|禁用|label label-default|<?php   echo webUrl('store/index/status',array('is_stop'=>1,'account_id'=>$row['account_id']))?>'
                            data-switch-value1='1|启用|label label-primary|<?php   echo webUrl('store/index/status',array('is_stop'=>0,'account_id'=>$row['account_id']))?>'
                            >
                            <?php  if($row['is_stop']==1) { ?>禁用<?php  } else { ?>正常<?php  } ?>
                        </td>
                        <td>
                            <a href="<?php  echo webUrl('member/list',array('account_id'=>$row['account_id']));?>"><?php  echo $row['member_num'];?></a>

                        </td>
                        <td>
                            <a href="<?php  echo webUrl('goods/index',array('account_id'=>$row['account_id']));?>"><?php  echo $row['account_num'];?></a>
                        </td>
                        <td>
                            <a href="<?php  echo webUrl('push/index',array('account_id'=>$row['account_id']));?>"><?php  echo $row['active_num'];?></a>
                        </td>
                        <td>
                          <?php if(cv('store.edit|store.view')) { ?>
                              <a class='btn btn-default btn-sm btn-op btn-operation' href="<?php  echo webUrl('store/edit', array('ac_id' => $row['account_id']))?>">
                                  <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php if(cv('shop.verify.store.edit')) { ?>编辑<?php  } else { ?>查看<?php  } ?>">
                                      <?php if(cv('shop.verify.store.edit')) { ?>
                                        <i class="icow icow-bianji2"></i>
                                      <?php  } else { ?>
                                        <i class="icow icow-chakan-copy"></i>
                                      <?php  } ?>
                                 </span>
                              </a>
                             <?php  } ?>
                            <?php if(cv('store.delete')) { ?>
                            <a class='btn btn-default  btn-sm btn-op btn-operation' data-toggle="ajaxRemove"  href="<?php  echo webUrl('store/delete', array('account_id' => $row['account_id']))?>" data-confirm="确认删除此单位吗？">
                                  <span data-toggle="tooltip" data-placement="top" title="" data-original-title="删除">
                                     <i class='icow icow-shanchu1'></i>
                                </span>
                            </a>
                            <?php  } ?>
                        </td>

                    </tr>
                    <?php  } } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td colspan="2">
                            <div class="btn-group">
                                <?php if(cv('store.edit')) { ?>
                                <button class="btn btn-default btn-sm btn-operation" type="button" data-toggle='batch' data-href="<?php  echo webUrl('store/status',array('status'=>1))?>">
                                    <i class='icow icow-qiyong'></i> 启用
                                </button>
                                <button class="btn btn-default btn-sm btn-operation" type="button" data-toggle='batch'  data-href="<?php  echo webUrl('store/status',array('status'=>0))?>">
                                    <i class='icow icow-jinyong'></i> 禁用
                                </button>
                                <?php  } ?>
                                <?php if(cv('store.delete')) { ?>
                                <button class="btn btn-default btn-sm btn-operation" type="button" data-toggle='batch-remove' data-confirm="确认要删除?" data-href="<?php  echo webUrl('store/delete')?>">
                                    <i class='icow icow-shanchu1'></i> 删除
                                </button>
                                <?php  } ?>
                            </div>
                        </td>
                        <td colspan="5" class="text-right"> <?php  echo $pager;?></td>
                    </tr>
                </tfoot>
            </table>
 
       </form>


        <?php  } else { ?>
<div class='panel panel-default'>
	<div class='panel-body' style='text-align: center;padding:30px;'>
		 暂时没有任何门店!
	</div>
</div>
<?php  } ?>
    </div>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>
<!--OTEzNzAyMDIzNTAzMjQyOTE0-->