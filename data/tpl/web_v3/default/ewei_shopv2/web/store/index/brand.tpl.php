<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>

<style type='text/css' xmlns="http://www.w3.org/1999/html">
    .dd-handle { height: 40px; line-height: 30px}
    .dd-list { width:860px;}
    .dd-handle span {
        font-weight: normal;
    }
</style>

<div class="page-header">
    当前位置：<span class="text-primary">商品品牌</span>
</div>
<div class="page-content">
    <div class="page-toolbar">
            <span class="pull-left">
                <a href="<?php  echo webUrl('store/index/brandadd')?>" class="btn btn-primary"><i class="fa fa-plus"></i> 添加新品牌</a>
            </span>
        <div class="input-group"></div>
    </div>
    <form action="" method="post" class="form-validate">
        <div class="dd" id="div_nestable">
            <ol class="dd-list">
                <?php  if(is_array($brands)) { foreach($brands as $row) { ?>
                <li class="dd-item full" data-id="<?php  echo $row['brand_id'];?>">
                    <div class="dd-handle" >
                        [ID: <?php  echo $row['brand_id'];?>]  <?php  echo $row['brand_name'];?>
                        <span class="pull-right">
                            <?php if(cv('store.index.brandedit|goods.category.view')) { ?>
                            <a class='btn btn-default btn-sm btn-operation btn-op' href="<?php  echo webUrl('store/index/brandedit', array('brand_id' => $row['brand_id']))?>"  >
                               <span data-toggle="tooltip" data-placement="top"  data-original-title="<?php if(cv('store.index.brandedit')) { ?>修改<?php  } else { ?>查看<?php  } ?>">
                                 <i class="icow icow-bianji2"></i>
                               </span>
                            </a>
                            <?php  } ?>
                            <?php if(cv('store.index.branddelete')) { ?>
                                <a class='btn btn-default btn-sm btn-operation btn-op' data-toggle='ajaxPost' href="<?php  echo webUrl('store/index/branddelete', array('brand_id' => $row['brand_id']))?>" data-confirm='确认删除此品牌吗？'>
                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="删除">
                                        <i class="icow icow-shanchu1"></i>
                                    </span>
                                </a>
                            <?php  } ?>
                        </span>
                    </div>
                 </li>
                <?php  } } ?>
           </ol>
        </div>
    </form>
</div>

<script language='javascript'>
    myrequire(['jquery.nestable'], function () {

        $('#btnExpand').click(function () {
            var action = $(this).data('action');
            if (action === 'expand') {
                $('#div_nestable').nestable('collapseAll');
                $(this).data('action', 'collapse').html('<i class="fa fa-angle-up"></i> 展开所有');

            } else {
                $('#div_nestable').nestable('expandAll');
                $(this).data('action', 'expand').html('<i class="fa fa-angle-down"></i> 折叠所有');
            }
        })
        var depth = <?php  echo intval($_W['shopset']['category']['level'])?>;
        if (depth <= 0) {
            depth = 2;
        }
        $('#div_nestable').nestable({maxDepth: depth});

        $('.dd-item').addClass('full');

        $(".dd-handle a,.dd-handle div").mousedown(function (e) {

            e.stopPropagation();
        });
        var $expand = false;
        $('#nestableMenu').on('click', function (e)
        {
            if ($expand) {
                $expand = false;
                $('.dd').nestable('expandAll');
            } else {
                $expand = true;
                $('.dd').nestable('collapseAll');
            }
        });

        $('form').submit(function(){
            var json = window.JSON.stringify($('#div_nestable').nestable("serialize"));
            $(':input[name=datas]').val(json);
        });

    })
</script>

<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>

