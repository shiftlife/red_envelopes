<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>

<div class="page-header">
	当前位置：
	<span class="text-primary">
		添加商品品牌
	</span>
</div>

<div class="page-content">
	<div class="page-sub-toolbar">
		<span class=''>
			<?php if(cv('goods.category.add')) { ?>
				<a class="btn btn-primary btn-sm" href="<?php  echo webUrl('goods/category/add')?>">添加新分类</a>
				<?php  } ?>
		</span>
	</div>
<form action="" method="post" class="form-horizontal form-validate" enctype="multipart/form-data" >

	<div class="form-group">
		<label class="col-sm-2 control-label must">商品品牌</label>
		<div class="col-sm-9 col-xs-12">
			<input type="text" name="brand_name" class="form-control" data-rule-required='true' />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-9 col-xs-12">
			<input type="submit"  value="提交" class="btn btn-primary" />
			<input type="button" name="back" onclick='history.back()' <?php if(cv('store.index.brandadd|store.index.brandedit')) { ?>style='margin-left:10px;'<?php  } ?> value="返回列表" class="btn btn-default" />
		</div>
	</div>
</form>
</div>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>


<!--青岛易联互动网络科技有限公司-->