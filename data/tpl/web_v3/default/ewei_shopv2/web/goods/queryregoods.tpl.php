<?php defined('IN_IA') or exit('Access Denied');?><style>
    .disabled {
        outline: 0 none;
        cursor: default!important;
        opacity: .4;
        filer: alpha(opacity=40);
        -ms-pointer-events: none;
        pointer-events: none;
    }

</style>
<div style='max-height:800px;overflow:auto;min-width:850px;' class="goods">
    <table class="table table-hover" style="min-width:850px;">
        <thead>
        <td style="width: 40px"></td>
        <td style="width: 300px">商品名称</td>
        <td>条码</td>
        <td>规格</td>
        <td>价格</td>
        </thead>
        <tbody class="goods_tbody" >
        <?php  if(is_array($ds)) { foreach($ds as $row) { ?>
        <!--class='disabled'-->
        <tr >
            <td style="width: 40px">
                <input type='radio'<?php  if($row['goods_id'] == $_GPC['goods_id']) { ?>checked<?php  } ?> name="modal_goods" class="modal_goods" data-value="<?php  echo $row['sign_name'];?>" onchange="" value="<?php  echo $row['goods_id'];?>"/>
            </td>
            <td style="width: 300px"> <?php  echo $row['goods_name'];?></td>
            <td> <?php  echo $row['bar_code'];?></td>
            <td> <?php  echo $row['normal_size'];?></td>
            <td> <?php  echo $row['price'];?></td>
        </tr>
        <?php  } } ?>
        </tbody>
    </table>
    <span class='pull-right'>
         共<?php  echo $allpage;?>页，<?php  echo $total;?>条
         <a  href="javascript:;"  onclick="front_page1(<?php  echo $pindex;?>)" title="上一页"> 上一页</a>
         <span> <input type="number" min="1" value="<?php  echo $pindex;?>" style="width:50px" class="current_page_goods">页</span> <a href="javascript:;" onclick="skip_page1(<?php  echo $allpage;?>)">跳转</a>
         <a href="javascript:;"  title="下一页" onclick="next_page1(<?php  echo $pindex;?>,<?php  echo $allpage;?>)"> 下一页</a>
         <a href="javascript:;"  onclick="get_video1(<?php  echo $allpage;?>)">尾页</a>
    </span>
</div>
<script  type="text/javascript">
    function front_page1(page) {
        --page;
        if(page<=0){
            alert('没有上一页了');
            return false;
        }else{
            get_video1(page);
        }
        return false;
    }
    function next_page1(page,allpage) {
        page = page+1;
        if( page>allpage) {
            alert('没有下一页了');
            return false;
        }else{
            get_video1(page);
        }
        return false;

    }

    function skip_page1(allpage) {
        get_video1(0,allpage);
    }
    function get_video1(page,allpage) {
        if(!page){
            var page = $('.current_page_goods').val();
        }
        if( page>allpage) {
            alert('没有下一页了');
            return false;
        }else {
            $.get("<?php  echo webUrl('goods/queryregoods')?>", {
                keyword: $.trim($('#search-kwd-goods').val()),
                page: page,
            }, function (dat) {
                $('#module-menus-goods').html(dat);
            });
        }
    }
   //
   //  $(document).ready(function() {
   //      $.get("<?php  echo webUrl('goods/querygoods')?>", {
   //          keyword: $.trim($('#search-kwd-goods').val()),
   //          checkIds: allCheckId
   //      }, function (dat) {
   //          $('#module-menus-goods').html(dat);
   //      })
   //  });





</script>
