<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>

<div class="page-header">
	当前位置：
	<span class="text-primary">
		<?php  if(!empty($item['id'])) { ?>编辑<?php  } else { ?>添加<?php  } ?>商品分类 <small><?php  if(!empty($item['id'])) { ?>修改【<?php  echo $item['name'];?>】<?php  } ?></small>
	</span>
</div>

<div class="page-content">
	<div class="page-sub-toolbar">
		<span class=''>
			<?php if(cv('goods.category.add')) { ?>
				<a class="btn btn-primary btn-sm" href="<?php  echo webUrl('goods/category/add')?>">添加新分类</a>
				<?php  } ?>
		</span>
	</div>
<form  <?php if( ce('goods.category' ,$item) ) { ?>action="" method="post"<?php  } ?> class="form-horizontal form-validate" enctype="multipart/form-data" >

	<div class="form-group">
		<label class="col-sm-2 control-label must">分类名称</label>
		<div class="col-sm-9 col-xs-12">
			<input type="text" name="type_name" class="form-control" value="<?php  echo $catInfo['type_name'];?>" data-rule-required='true' />
			<input type="hidden" name="cat_id" class="form-control" value="<?php  echo $catInfo['cat_id'];?>" data-rule-required='true' />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-9 col-xs-12">
			<?php if( ce('goods.category' ,$item) ) { ?>
			<input type="submit"  value="提交" class="btn btn-primary" />
			<?php  } ?>
			<input type="button" name="back" onclick='history.back()' <?php if(cv('goods.category.add|goods.category.edit')) { ?>style='margin-left:10px;'<?php  } ?> value="返回列表" class="btn btn-default" />
		</div>
	</div>
</form>
</div>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>


<!--青岛易联互动网络科技有限公司-->