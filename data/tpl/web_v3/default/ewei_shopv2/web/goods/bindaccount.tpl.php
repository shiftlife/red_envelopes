<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>
<style>
    .tc{
        text-align: center;
        background: white;
    }
    .iconfont{
        font-size: 20px;
        cursor: pointer;
        text-align: center;
        font-weight: bold;
     }
    .iconfont:hover {
        color: #13c19f;
        background: #fff;
     }
    .ant-input-number-input {
        width: 100%;
        text-align: left;
        outline: 0;
        -moz-appearance: textfield;
        height: 35px;
        transition: all .3s linear;
        color: #415161;
        background-color: #fff;
        border:0.7px solid #6f6f6f;
        border-radius: 3px;
        padding: 0 7px;

    }
    .ant-input-number{
        height: 34px;
    }
</style>
<div class="page-header">
    当前位置：<span class="text-primary">门店管理</span>
</div>

<div id="infoModal"  class="modal fade" tabindex="-1">
    <div class="modal-dialog" style='width: 920px;'>
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h3>选择单位</h3>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="input-group">
                        <input type="text" class="form-control" name="keyword" value="" id="search-kwd-notice" placeholder="请输入单位编码/单位名称" />
                        <span class='input-group-btn'><button type="button" class="btn btn-default" onclick="search()">搜索</button></span>
                    </div>
                </div>
                <div id="module-menus-notice" style="padding-top:5px;"></div>
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0);" onclick="choose_account()" class="btn btn-default" data-dismiss="modal"
                   aria-hidden="true">确认</a>
                <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>

    </div>
</div>
<div id="modal-module-menus-notice"  class="modal fade" tabindex="-1">
    <div class="modal-dialog" style='width: 920px;'>
        <div class="modal-content">
            <div class="modal-header"><button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button><h3>选择商品</h3></div>
            <div class="modal-body" >
                <div class="row">
                    <div class="input-group">
                        <input type="text" class="form-control" name="keyword" value="" id="search-kwd-goods" placeholder="请输入商品关键字" />
                        <span class='input-group-btn'><button type="button" class="btn btn-default" onclick="search_goods()">搜索</button></span>
                    </div>
                </div>
                <div id="module-menus-goods" style="padding-top:5px;"></div>
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0);" onclick="choose_goods()" class="btn btn-default" data-dismiss="modal"
                   aria-hidden="true">确认</a>
                <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>

    </div>
</div>
<div class="page-content">
    <div class="page-table-header">
        <form action="" method="post" class="form-horizontal form-validate">

        <div class="form-group">
              <label class="col-lg control-label must">选择单位</label>
              <div class="col-sm-3 col-xs-12">
                  <input type="hidden" class="form-control account_id" name="account_id">
                  <input type="text" class="form-control sign_name" onclick="popwin = $('#infoModal').modal();" value="请选择" >
                  <div class="has-error" style="margin-top: 10px;display: none;color:#f96868">单位不能为空</div>
              </div>
          </div>
        </form>
    </div>
    <table class="table table-responsive">
        <thead>
        <tr>
            <th style="width: 50px; max-width: 50px; text-align: center;"></th>
            <th style="width: 50px; max-width: 50px; text-align: center;"></th>
            <th style="width: 100px;">商品编码</th>
            <th style="width: 300px;">商品名称</th>
            <th style="width: 50px;"></th>
            <th style="width: 180px;">商品规格</th>
            <th style="width: 100px;">价格</th>
            <th style="width: 100px;">库存数量</th>
            <th style="width: 100px;">入库数量</th>
        </tr>
        </thead>
        <tbody class="body_data">
        <tr>
            <td style="width: 50px; max-width: 50px; text-align: center;">
                <div>1</div>
            </td>
            <td style="width: 50px; max-width: 50px;">
                <div class="tc">
                    <i class="iconfont icon-plus common-op" onclick="add_tr()">+</i><i class="iconfont icon-reduce common-op"  onclick="reduce_tr()">-</i>
                </div>
            </td>
            <td class="autocomplete-column bar_code"  style="width: 100px;"></td>
            <td></td>
            <td style="width: 300px;">
                <input type="hidden" class="form-control goods_id" name="goods_id">
                <button type="button" style="background: white" onclick="goodsmodule()"><i class="iconfont">...</i></button>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
    <label class="tip" style="display: none ;color:#f96868">商品不能为空并要填写对应的数量</label>
    <button  class="btn btn-primary btn-sm" onclick="bindaccount()">提交</button>
</div>

<script type="text/javascript">
    //选择单位点击确定按钮
    function choose_account() {
        var check = $('.modal_account');
        var account_id;
        var sign_name;
        $(check).each(function () {
            if ($(this).prop('checked')) {
                account_id = $(this).val();
                sign_name = $(this).attr('data-value');
            }
        });
        $('.account_id').val(account_id);
        $('.sign_name').val(sign_name);
        if($('.account_id').val()){
            $('.has-error').css({'display':'none'});
            $('.sign_name').css({'border':''});
        }
    }
    //选择商品点击确定按钮
    function choose_goods() {
        $('.goods_id').val(allCheckId);
        $.get("<?php  echo webUrl('goods/goods_detail')?>", {
            goods_ids:allCheckId,
            account_id:$('.account_id').val()
        }, function (dat) {
            dat = JSON.parse(dat);
            result = dat['result'];
            var html = '';
            $('.body_data').find('tr').remove();
            for(var i=0;i<result['goods'].length;i++){
                var len = $('.body_data').find('tr').length+1;
                html = '<tr data-value='+result['goods'][i]['goods_id']+'><td style="width: 50px; max-width: 50px; text-align: center;">\n' +
                    '                <div>' +len+
                    '</div></td>' +
                    '<td style="width: 50px; max-width: 50px;"><div> <div class="tc"><i class="iconfont icon-plus common-op" onclick="add_tr()">+</i><i class="iconfont icon-reduce common-op" onclick="reduce_tr()">-</i> </div> </div> </td>' +
                    '<td class="autocomplete-column bar_code"  style="width: 100px;">'+result['goods'][i]['bar_code']+' </td> ' +
                    '<td style="width: 300px;">'+result['goods'][i]['goods_name']+'</td>' +
                    '<td><button type="button" style="background: white" onclick="goodsmodule()"><i class="iconfont">...</i></button> </td> ' +
                    '<td>'+result['goods'][i]['normal_size']+'</td> ' +
                    '<td>'+result['goods'][i]['price']+'</td> ' +
                    '<td >'+result['goods'][i]['amount']+'</td> ' +
                    '<td><input type="number"  class="ant-input-number-input goods_'+result['goods'][i]['goods_id']+'"  placeholder="输入入库数量"></td> ' +
                    '</tr>';
                $('.body_data').append(html);
            }
        });
        if($('.body_data').html()){
            $('.table-responsive').css({'border':'' })
            $('.tip').css({'display':'none'})
        }

    }
    //点击商品出现模态框
    function goodsmodule() {
        var account_id = $('.account_id').val();
        if(account_id){
            popwin = $('#modal-module-menus-notice').modal();
        }else{
            $('.sign_name').css({'border':'solid #f96868', 'border-width':'1px' });
            $('.has-error').css({'display':'block'})
        }
    }

    $(function () {
        $.get("<?php  echo webUrl('goods/query')?>", {
            keyword: $.trim($('#search-kwd-notice').val()),
            checkId:accountId
        }, function (dat) {
            $('#module-menus-notice').html(dat);
        });
        $.get("<?php  echo webUrl('goods/querygoods')?>", {
            keyword: $.trim($('#search-kwd-goods').val()),
            checkIds:allCheckId
        }, function (dat) {
            $('#module-menus-goods').html(dat);
        });
    });
    //商品模态框展示时重载数据
    $("#modal-module-menus-notice").on("show.bs.modal",function(){
        $.get("<?php  echo webUrl('goods/querygoods')?>", {
            keyword: $.trim($('#search-kwd-goods').val()),
            checkIds:allCheckId
        }, function (dat) {
            $('#module-menus-goods').html(dat);
        });
    });
    //单位数据搜索
     function search() {
        if ($.trim($('#search-kwd-notice').val()) == '') {
            Tip.focus('#search-kwd-notice', '请输入关键词');
            return;
        }
        $("#module-menus-notice").html("正在搜索....");
        $.get("<?php  echo webUrl('goods/query')?>", {
            keyword: $.trim($('#search-kwd-notice').val()),
            checkId:accountId
        }, function (dat) {
            $('#module-menus-notice').html(dat);
        });

    }
    //商品数据搜索
    function search_goods() {
        if ($.trim($('#search-kwd-goods').val()) == '') {
            Tip.focus('#search-kwd-goods', '请输入关键词');
            return;
        }
        $("#module-menus-goods").html("正在搜索....");
        $.get("<?php  echo webUrl('goods/querygoods')?>", {
            keyword: $.trim($('#search-kwd-goods').val()),
            checkIds:allCheckId
        }, function (dat) {
            $('#module-menus-goods').html(dat);
        });
    }
    function add_tr() {
        if($("table tr:last").find('.bar_code').text().trim()==''){
            $('.table-responsive').css({'border':'solid #f96868', 'border-width':'2px' });
            $('.tip').css({'display':'block'});
            return false;
        }
        var len = $('.body_data').find('tr').length+1;
        var html = '<tr><td style="width: 50px; max-width: 50px; text-align: center;">\n' +
            '                <div>' +len+
            '</div></td>' +
            '<td style="width: 50px; max-width: 50px;"><div> <div class="tc"><i class="iconfont icon-plus common-op" onclick="add_tr()">+</i><i class="iconfont icon-reduce common-op" onclick="reduce_tr()">-</i> </div> </div> </td>' +
            '<td class="autocomplete-column bar_code"  style="width: 100px;"> </td> ' +
            '<td style="width: 300px;"></td>' +
            '<td><button type="button" style="background: white" onclick="goodsmodule()"><i class="iconfont">...</i></button> </td> ' +
            '<td></td> ' +
            '<td></td> ' +
            '<td></td> ' +
            '<td></td> ' +
            '</tr>';

        $('.body_data').append(html);

    }
    function reduce_tr() {
        var goods_id = ','+$("table tr:last").attr('data-value')+',';
        var index = allCheckId.indexOf(goods_id);
        var len = goods_id.length;
        var last = allCheckId.lastIndexOf(',');
        allCheckId =  allCheckId.substring(0,index)+','+allCheckId.substring(index+len-1,last);
        if($('.body_data').find('tr').length>1){
            $("table tr:last").remove();
            $('.table-responsive').css({'border':''})
            $('.tip').css({'display':'none'})
        }else{
            $('.table-responsive').css({'border':'solid #f96868', 'border-width':'2px' })
            $('.tip').css({'display':'block'})
        }
    }
     var allCheckId = ',';
     var accountId = '';
     $('html').on('click','.modal_goods',function(){
         if ($(this).prop('checked')) {
             allCheckId += $(this).val() + ',';
         }else{
             var goods_id =  ','+ $(this).val() +',';
             var index = allCheckId.indexOf(goods_id);
             var len = goods_id.length;
             var last = allCheckId.lastIndexOf(',');
             allCheckId =  allCheckId.substring(0,index)+','+allCheckId.substring(index+len,last+1);
         }
     });

    $('html').on('click','.modal_account',function(){
        accountId = $(this).val();
    });
    //提交数据
    function bindaccount() {
        var account_id = $('.account_id').val();
        if ($("table tr:last").find('.bar_code').text().trim() == '') {
            tip.msgbox.err('商品不能为空!' || tip.lang.error);
            return false;
        }
        var goods_ids = allCheckId.split(",");
        goods_ids = $.grep(goods_ids,function(n,i){
            return n;
        },false);
        var amount = '';
        for(var i = 0;i<goods_ids.length;i++){
            var class_str = '.goods_'+goods_ids[i];
            var a = $(class_str).val();
            if(!a){
                a = 0;
            }
            amount += a +',';
        }
        $.post("<?php  echo webUrl('goods/bindaccount')?>", {
            goods_ids:allCheckId,
            account_id: account_id,
            amount:amount
        }, function (dat) {
            // if(dat.status == 1){
                location.href = "<?php  echo webUrl('goods/record')?>";
        });
    }

</script>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>
