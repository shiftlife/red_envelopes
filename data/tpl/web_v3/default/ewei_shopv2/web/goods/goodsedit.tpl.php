<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_header', TEMPLATE_INCLUDEPATH)) : (include template('_header', TEMPLATE_INCLUDEPATH));?>

<div class="page-header">

	当前位置：<span class="text-primary"><?php  if(!empty($item['id'])) { ?>编辑<?php  } else { ?>添加<?php  } ?>店员 <small><?php  if(!empty($item['id'])) { ?>修改【<?php  echo $item['salername'];?>】<?php  } ?></small></span>

</div>



<div class="page-content">

    <?php if(cv('store.saler.add')) { ?>

    <div class="page-sub-toolbar">

        <a class="btn btn-primary btn-sm" href="<?php  echo webUrl('store/goodsedit')?>">添加新商品
        </a>

    </div>

    <?php  } ?>

    <form {action="" method="post"class="form-horizontal form-validate" enctype="multipart/form-data">
        <input type="hidden" name="goods_id" value="<?php  echo $item['goods_id'];?>" />
            <div class="form-group">
                <label class="col-lg control-label must">商品名称</label>
                <div class="col-sm-9 col-xs-12">
                    <input type="text" name="goods_name" class="form-control" value="<?php  echo $item['goods_name'];?>" data-rule-required='true'/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg control-label must">商品分类</label>
                <div class="col-sm-9 col-xs-12">
                    <select name="cat_id" class='form-control input-sm select-sm select2' style="width:200px;">
                        <option value="">请选择分类</option>
                        <?php  if(is_array($categreys)) { foreach($categreys as $key => $value) { ?>
                        <option value="<?php  echo $value['cat_id'];?>" <?php  if($item['cat_id'] == $value['cat_id']) { ?>selected<?php  } ?>><?php  echo $value['type_name'];?></option>
                        <?php  } } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg control-label must">商品品牌</label>
                <div class="col-sm-9 col-xs-12">
                    <select name="brand_id" class='form-control input-sm select-sm select2' style="width:200px;">
                        <option value="">请选择品牌</option>
                        <?php  if(is_array($brands)) { foreach($brands as $key => $value) { ?>
                        <option value="<?php  echo $value['brand_id'];?>" <?php  if($item['brand_id'] == $value['brand_id']) { ?>selected<?php  } ?>><?php  echo $value['brand_name'];?></option>
                        <?php  } } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg control-label">商品图片</label>
                <div class="col-sm-9 col-xs-12">
                    <?php  echo tpl_form_field_image2('thumb', $item['thumb'])?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg control-label must">条码</label>
                <div class="col-sm-9 col-xs-12">
                    <input type="text" name="bar_code" class="form-control" value="<?php  echo $item['bar_code'];?>" data-rule-required='true'/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg control-label must">价格</label>
                <div class="col-sm-9 col-xs-12">
                    <input type="text" name="price" class="form-control" value="<?php  echo $item['price'];?>"/>
                </div>
            </div>
             <div class="form-group">
                 <label class="col-lg control-label">规格</label>
                 <div class="col-sm-9 col-xs-12">
                     <input type="text" name="normal_size" class="form-control" value="<?php  echo $item['normal_size'];?>" />
                 </div>
             </div>
            <div class="form-group">
                <label class="col-lg control-label">状态</label>
                <div class="col-sm-9 col-xs-12">
                    <label class='radio-inline'>
                        <input type='radio' name='status' value=1 <?php  if($item['status']==1) { ?>checked<?php  } ?> /> 上架
                    </label>
                    <label class='radio-inline'>
                        <input type='radio' name='status' value=0 <?php  if($item['status']==0) { ?>checked<?php  } ?> /> 下架
                    </label>
                </div>
            </div>



           <div class="form-group"></div>

            <div class="form-group">

                    <label class="col-lg control-label"></label>

                    <div class="col-sm-9 col-xs-12">

                           <?php if( ce('store.saler' ,$item) ) { ?>

                            <input type="submit" value="提交" class="btn btn-primary"  />



                        <?php  } ?>

                       <input type="button" name="back" onclick='history.back()' value="返回列表" class="btn btn-default" />

                    </div>

            </div>

    </form>

</div>



<script language='javascript'>



    function search_users() {

        $("#module-menus1").html("正在搜索....")

        $.get('<?php  echo webUrl("store/perm/role/query")?>', {

            keyword: $.trim($('#search-kwd1').val())

        }, function(dat){

            $('#module-menus1').html(dat);

        });

    }



    function select_role(o) {

        $("#userid").val(o.id);

        $("#user").val( o.rolename );

        $(".close2").click();

    }

</script>



<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('_footer', TEMPLATE_INCLUDEPATH)) : (include template('_footer', TEMPLATE_INCLUDEPATH));?>

